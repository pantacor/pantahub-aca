// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package storage

import (
	"log"
	"os"
	"time"

	"gitlab.com/pantacor/pantahub-aca/utils"

	mgo "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	collectionPrefix = "pantahub_aca_"
)

var (
	defaultStorage Storage
)

// Storage define all storage action and methods
type Storage struct {
	session         *mgo.Session
	timeoutDuration time.Duration
}

// Close close session with storage
func (s *Storage) Close() {
	s.session.Close()
}

// IsNotFound resource not found
func (s *Storage) IsNotFound(err error) bool {
	return err == mgo.ErrNotFound
}

// IsDuplicateKey test if a key already exist on storage
func (s *Storage) IsDuplicateKey(err error) bool {
	lastError, ok := err.(*mgo.LastError)
	if !ok {
		return false
	}

	return lastError.Code == 11000
}

func (s *Storage) newID() *bson.ObjectId {
	id := bson.NewObjectId()
	return &id
}

func (s *Storage) getCollection(name string) *mgo.Collection {
	name = collectionPrefix + name
	return s.session.DB("").C(name)
}

func (s *Storage) getDbCollectionRaw(db, name string) *mgo.Collection {
	return s.session.DB(db).C(name)
}

// NewStorage create new Storage Struct
func NewStorage() Storage {
	if defaultStorage.session != nil {
		return defaultStorage
	}

	session, err := utils.GetMongoSession()
	if err != nil {
		panic(err)
	}

	if utils.GetEnv("PANTAHUB_ACA_DEBUG_MGO", "") != "" {
		mgo.SetLogger(log.New(os.Stdout, "", 0))
		mgo.SetDebug(true)
	}

	timeout, err := time.ParseDuration(utils.GetEnv("PANTAHUB_ACA_TIMEOUT_DURATION", "30m"))
	if err != nil {
		panic(err)
	}

	defaultStorage = Storage{
		session:         session,
		timeoutDuration: timeout,
	}
	return defaultStorage
}
