// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package storage

import (
	"errors"
	"time"

	mgo "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/sirupsen/logrus"
	"gitlab.com/pantacor/pantahub-aca/models"
	"gitlab.com/pantacor/pantahub-aca/utils"
)

func (s *Storage) getCertsCollection() *mgo.Collection {
	collection := s.getCollection("certs")

	prnIndex := mgo.Index{
		Key:        []string{"prn"},
		Unique:     true,
		Background: true,
		Sparse:     false,
	}

	err := collection.EnsureIndex(prnIndex)
	if err != nil {
		logrus.WithError(err).Error("Error when ensuring prn index of certs collection")
		return nil
	}

	return collection
}

// DeleteCert delete certificate of database
func (s *Storage) DeleteCert(id bson.ObjectId) error {
	certs := s.getCertsCollection()

	deletedAt := time.Now()
	update := bson.M{
		"$set": bson.M{
			"deleted_at": &deletedAt,
		},
	}
	query := bson.M{
		"_id":        id,
		"deleted_at": nil,
	}
	err := certs.Update(query, update)
	if err != nil {
		return err
	}

	return nil
}

// FindCert find certificate on database
func (s *Storage) FindCert(id bson.ObjectId) (models.Cert, error) {
	var result models.Cert
	certs := s.getCertsCollection()

	err := certs.Find(map[string]interface{}{
		"_id":        id,
		"deleted_at": nil,
	}).One(&result)
	if err != nil {
		return result, err
	}

	if result.ID == nil {
		return result, errors.New("Certificate not found")
	}

	return result, nil
}

// InsertCert insert new cert on database
func (s *Storage) InsertCert(cert *models.Cert) error {
	certs := s.getCertsCollection()

	if cert.ID == nil {
		cert.ID = s.newID()
	}
	if cert.PRN == "" {
		cert.PRN = utils.IDGetPrn(bson.ObjectId(*cert.ID), models.CertsCollectionName)
	}

	return certs.Insert(cert)
}

// CreateCert create new certificate
func (s *Storage) CreateCert(cert *models.Cert) *models.Cert {
	createdAt := time.Now()
	cert.ID = s.newID()
	cert.PRN = utils.IDGetPrn(bson.ObjectId(*cert.ID), models.CertsCollectionName)
	cert.CreatedAt = &createdAt
	cert.DeletedAt = nil
	cert.UpdatedAt = nil
	return cert
}
