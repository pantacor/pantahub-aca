// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/pantacor/pantahub-aca/api"
	"gitlab.com/pantacor/pantahub-aca/utils"
)

func main() {
	serverAddress := utils.GetEnv("PANTAHUB_ACA_SERVER_HOST", ":12368")
	server := api.NewServer()
	server.Run(serverAddress)
}

func init() {
	out, err := os.OpenFile("/proc/1/fd/1", os.O_WRONLY, 0644)
	if err != nil {
		out = os.Stdout
		log.Warning("Using os.Stdout")
	}

	log.SetOutput(out)
	log.SetLevel(log.DebugLevel)
	log.SetReportCaller(true)
}
