// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package pantahub

import "time"

// VerifyOwnershipReq pantahub verify token ownership
type VerifyOwnershipReq struct {
	Service    string `json:"service"`
	TokenID    string `json:"token-id"`
	Owner      string `json:"owner"`
	IDevIDName string `json:"idevid-name"`
	Signature  string `json:"signature"`
}

// RegisterIDevIDReq register device request payload
type RegisterIDevIDReq struct {
	Cert       string `json:"csr"`
	Name       string `json:"name"`
	DeviceName string `json:"device-name"`
}

type deviceAPI struct {
	ID           string                 `json:"id"`
	Prn          string                 `json:"prn"`
	Nick         string                 `json:"nick"`
	Owner        string                 `json:"owner"`
	OwnerNick    string                 `json:"owner-nick,omitempty"`
	Secret       string                 `json:"secret,omitempty"`
	TimeCreated  time.Time              `json:"time-created"`
	TimeModified time.Time              `json:"time-modified"`
	Challenge    string                 `json:"challenge,omitempty"`
	IsPublic     bool                   `json:"public"`
	UserMeta     map[string]interface{} `json:"user-meta"`
	DeviceMeta   map[string]interface{} `json:"device-meta"`
	Garbage      bool                   `json:"garbage"`
}

// RegisterIDevIDRes register device response payload
type RegisterIDevIDRes struct {
	Cert   string     `json:"crt"`
	Device *deviceAPI `json:"device"`
}
