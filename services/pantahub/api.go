// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package pantahub

import (
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/google/go-tpm/tpm2"
	"github.com/google/uuid"
	"gitlab.com/pantacor/pantahub-aca/utils"
	"gopkg.in/resty.v1"
)

const (
	sigVerifyURL = "/auth/signature/verify"
	regIDevIDURL = "/devices/register"
	regLDevIDURL = "/devices/issue"
)

// ValidateOwnership validate ownership of a certificate on pantahub
func ValidateOwnership(exts *utils.PHExtensions, public tpm2.Public) (string, error) {
	iDevName, err := public.Name()
	if err != nil {
		return "", err
	}

	name := utils.Tpm2NameToString(iDevName)

	// validate ownership claim - if any.
	// The will of owner to care for device is expressed through
	// a signature of the tpm name using a validateautotok
	// format of that signature binary is: SIG(HEX($autotok.prn)-$idevid_name)
	body := VerifyOwnershipReq{
		Service:    "api.pantahub.com",
		IDevIDName: name,
		Signature:  base64.StdEncoding.EncodeToString(exts.NameSigByOwner),
		TokenID:    string(exts.OwnerTokenID),
		Owner:      exts.Owner,
	}

	response, err := resty.R().SetBody(body).Post(builtURL(sigVerifyURL))
	if err != nil {
		return "", err
	}

	if response.StatusCode() != http.StatusOK {
		errid := uuid.New().String()
		errMsg := fmt.Sprintf("ERROR [%s]: remote error validating ownership - %s\n", errid, response.Body())
		return "", errors.New(errMsg)
	}

	return name, nil
}

// RegisterIDevID register and new device on pantahub using the IDevID
func RegisterIDevID(cert *x509.CertificateRequest, iDevIDName, akName string) (*RegisterIDevIDRes, error) {
	payload := RegisterIDevIDReq{
		Cert:       base64.StdEncoding.EncodeToString(cert.Raw),
		Name:       iDevIDName,
		DeviceName: akName,
	}

	response, err := resty.R().SetBody(payload).Post(builtURL(regIDevIDURL))
	if err != nil {
		return nil, err
	}

	if response.StatusCode() != http.StatusOK {
		errid := uuid.New().String()
		errMsg := fmt.Sprintf("ERROR [%s]: remote CA error - %s\n", errid, response.Body())
		return nil, errors.New(errMsg)
	}

	respJSON := &RegisterIDevIDRes{}
	body := response.Body()
	err = json.Unmarshal(body, respJSON)
	if err != nil {
		return nil, err
	}

	return respJSON, nil
}

func builtURL(path string) string {
	return utils.Env.BaseAPIScheme + "://" + utils.Env.BaseAPIHost + path
}
