# Pantahub ACA

### Running

```bash
$ docker-compose up -d
```

Access http://localhost:12368


## Client 

In order to test the basic client, you can run client/enroll.sh, that will save on 0x1c00003 the certificate sign by the ACA server

```bash
client/enroll.sh

tpm2_nvread -C o 0x1c00003 # the certificate format is der
```

#### Is you use the TPM2 simulator

In order to run the client/enroll.sh script you need to setup your simulator, for that you can run 

```bash
client/setup-simulator.sh 
```

And that will configurate everything you need in the simulator

#### Client test

You can run a docker image with all the TPM2 and other dependencies required to do the enrollment process

```bash
docker pull registry.gitlab.com/pantacor/pantahub-aca/client
docker run --rm -it --device=/dev/tpm0 --device=/dev/tpmrm0 -e "ACA_URL=https://api.aca.pantahub.com" -v ${PWD}/tpm2:/tpm2 registry.gitlab.com/pantacor/pantahub-aca/client
```

This image doesn't have a TPM2 simulator inside that why yo need to connect with the respective TPM device

If you want to run the container agains a TPM simulator you can pass the TPM2TOOLS_TCTI enviroment variable

```
docker pull registry.gitlab.com/pantacor/pantahub-aca/client

docker run --rm -it -e "TPM2TOOLS_TCTI=mssim:host=HOST_IP,port=2321" -e "DEBUG=true" -e "ACA_URL=https://api.aca.pantahub.com" -v ${PWD}/tpm2:/tpm2 registry.gitlab.com/pantacor/pantahub-aca/client 
```

## Deployment

In order to use register endpoint you need to have tls client cert for the client.

When deploying pantahub-aca behind a proxy you need to ensure that the following things happen:

 1. proxy must add a secret to ```PhProxyTlsToken``` http header. The secret can be shared with
    backend through "PANTAHUB_PROXY_TLS_AUTH_TOKEN" env variable and must not be known to anyone
    but hte proxy and the backend.
 2. trusted proxy adds the client cert retrieved to http header: PhClientCertificate
 
 If ACA is not running behind a proxy it can directly accept TLS connections and extract the
 needed info from there instead of headers.

For local development you can run nginx with the following command:

```
# update nginx-conf.d/ to have the proper host interface IP as upstream target
docker run -p 8443:443  -v $PWD/nginx-certs.d:/etc/nginx/certs.d -v $PWD/nginx-conf.d/:/etc/nginx/conf.d/ --name nginx --rm nginx:alpine

```

