#!/bin/bash

source $(dirname $0)/const.sh
debugger

if [ -z "$1" ]; then
  TMP_FOLDER=$(mktemp -d /tmp/enroll.XXXXXX)
else
  TMP_FOLDER=$1
fi

mkdir -p $TMP_FOLDER
cd $TMP_FOLDER

CERTIFICATE_FILE_ENC="certificate.in"
CERTIFICATE_FILE="certificate.out"
CREDENTIAL_FILE="credential.in"
SESSION_FILE="session"
ACTIVATION_OUTPUT="credential.out"
EK_FILE="ek-cert.der"
EK_PUB_FILE="ek.pub"
AK_CTX="ak.ctx"
AK_PUB_FILE="ak.pub"
AK_NAME_FILE="ak.name"
AK_OUT="ak.out"
AK_PUB="ak.pub"
AK_TPM_PUB="ak.tpm.pub"
EK_TPM_PUB="ek.tpm.pub"

if ! tpm2_readpublic -c $EK_HANDLE; then
  echo "This device doesn't have the factory configuration."
  echo "Please run first the genfactory command."
  echo ""
  echo "./genfactory.sh OWNERPRN AUTOJOINTOKEN TOKEN_ID"
fi

if ! tpm2_readpublic -c $AK_HANDLE; then
  echo "This device doesn't have the factory configuration."
  echo "Please run first the genfactory command."
  echo ""
  echo "./genfactory.sh OWNERPRN AUTOJOINTOKEN TOKEN_ID"
fi

tpm2_readpublic -c $EK_HANDLE -o $EK_PUB_FILE

# read the cert for EK implanted by TPM vendor
if ! tpm2_nvread -C o -o $EK_FILE $EKCERT_NVHANDLE && [ ! -f $EK_FILE ]; then
  tpm2_getekcertificate -X -u $EK_PUB_FILE $EK_PORTAL | jq '.certificate' | sed -e 's/-/+/g' | sed -e 's/_/\//g' | sed -e 's/%3D/=/g' | sed -e 's/"//g' | base64 -d > $EK_FILE
fi

# Clean bad der format with spaces
openssl x509 -in $EK_FILE -inform der -outform der -out $EK_FILE

# Read EK and AK in full tpmt format
tpm2_readpublic -c $EK_HANDLE -o $EK_TPM_PUB -f tpmt
tpm2_readpublic -c $AK_HANDLE -o $AK_TPM_PUB -f tpmt

AK_NAME=$(tpm2_readpublic -c $AK_HANDLE | grep ^name: | awk '{ print $2 }')
AK_PUB=$(cat $AK_TPM_PUB | base64 -w 0)
EK_CERT=$(cat $EK_FILE | base64 -w 0)
EK_PUB=$(cat $EK_TPM_PUB | base64 -w 0)

curl -L -k --request POST \
  --url $ACA_URL/enroll/request/ \
  --header 'content-type: application/json' \
  --data "{ \"ak-pub\": \"${AK_PUB}\", \"ak-name\": \"${AK_NAME}\", \"ek-cert\": \"${EK_CERT}\", \"ek-pub\": \"${EK_PUB}\" }" > iak_result.json

cat iak_result.json | jq '.certificate' -r | base64 -d > $CERTIFICATE_FILE_ENC
cat iak_result.json | jq '.credential' -r | base64 -d > $CREDENTIAL_FILE
IV=$(cat iak_result.json | jq '.iv' -r)

tpm2_startauthsession --policy-session -S $SESSION_FILE
tpm2_policysecret -S $SESSION_FILE -c e
tpm2_activatecredential -Q -c $AK_HANDLE -C $EK_HANDLE -i $CREDENTIAL_FILE -o $ACTIVATION_OUTPUT -P"session:$SESSION_FILE"
tpm2_flushcontext $SESSION_FILE

krypto d -in $CERTIFICATE_FILE_ENC -out $CERTIFICATE_FILE -iv $IV -k "$(cat $ACTIVATION_OUTPUT)"

tpm2_nvundefine -Q $AIK_NV -C o || true
tpm2_nvdefine -Q $AIK_NV -C o -a "ownerread|policywrite|ownerwrite|no_da|policyread|authread"
tpm2_nvwrite -Q $AIK_NV -C o -i $CERTIFICATE_FILE

if [ "$DEBUG" == true ]; then
  tpm2_nvread -C o $AIK_NV | openssl x509 --inform der -text
fi

# Read AIK
tpm2_nvread -C o $AIK_NV | openssl x509 -inform der -out ak.crt -outform pem

# Read AK and IDEVID public with tpmt format
tpm2_readpublic -c $IDEVID_HANDLE -o idevid.pub -f tpmt
tpm2_readpublic -c $AK_HANDLE -o ak.pub -f tpmt

csr_base64=`tpm2_nvread -C o $IDEVID_NV | base64 -w 0`
akcert_hex=`cat ak.crt | xxd -c 10000 -p | sed -e 's/\ //g'`
akpub_hex=`cat ak.pub | xxd -c 10000 -p | sed -e 's/\ //g'`
idevidpub_hex=`cat idevid.pub | xxd -c 10000 -p | sed -e 's/\ //g'`

curl --engine tpm2tss \
	-k \
	-H "Content-Type: application/json" \
	-H "PhTpm2ClientIDevPub:${idevidpub_hex}" \
	-H "PhTpm2ClientIAKCert:${akcert_hex}" \
	-H "PhTpm2ClientIAKPub:${akpub_hex}" \
	-X POST \
	-d '{"csr" : "'${csr_base64}'"}' \
	${ACA_URL}/register > result.json

cat result.json | jq -r '.crt' | base64 -di | openssl x509 -inform der -out idevid.signed.crt --outform der

tpm2_nvundefine -Q $IDEVID_NV -C o || true
tpm2_nvdefine -Q $IDEVID_NV -C o -a "ownerread|policywrite|ownerwrite|no_da|policyread|authread"
tpm2_nvwrite -Q $IDEVID_NV -C o -i idevid.signed.crt

cd -
if [ ! "$DEBUG" == true ]; then
  rm -r $TMP_FOLDER
else
  tpm2_nvread -C o $IDEVID_NV | openssl x509 --inform der -text
fi

echo ""
echo "Enrollment complete IAK and IDevID cert are loaded"