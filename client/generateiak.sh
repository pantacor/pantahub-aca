#!/bin/bash

source $(dirname $0)/const.sh
debugger

if [ -z "$1" ]; then
  TMP_FOLDER=$(mktemp -d /tmp/enroll.XXXXXX)
else
  TMP_FOLDER=$1
fi

EK_LOADED="EK loaded from $EK_HANDLE"
EK_CREATED="New EK created on $EK_HANDLE"

CERTIFICATE_FILE_ENC="$TMP_FOLDER/certificate.in"
CERTIFICATE_FILE="$TMP_FOLDER/certificate.out"
CREDENTIAL_FILE="$TMP_FOLDER/credential.in"
SESSION_FILE="$TMP_FOLDER/session"
ACTIVATION_OUTPUT="$TMP_FOLDER/credential.out"
EK_FILE="$TMP_FOLDER/ek-cert.der"
EK_PUB_FILE="$TMP_FOLDER/ek.pub"
AK_CTX="$TMP_FOLDER/ak.ctx"
AK_PUB_FILE="$TMP_FOLDER/ak.pub"
AK_NAME_FILE="$TMP_FOLDER/ak.name"
AK_OUT="$TMP_FOLDER/ak.out"
AK_PUB="$TMP_FOLDER/ak.pub"
AK_TPM_PUB="$TMP_FOLDER/ak.tpm.pub"
EK_TPM_PUB="$TMP_FOLDER/ek.tpm.pub"

mkdir -p $TMP_FOLDER

# Read or create ek public key.
tpm2_readpublic -c $EK_HANDLE -o $EK_PUB_FILE || tpm2_createek -Q -c $EK_HANDLE -G rsa -u $EK_PUB_FILE
tpm2_readpublic -c $EK_HANDLE -o $EK_TPM_PUB -f tpmt

# read the cert for EK implanted by TPM vendor
if ! tpm2_nvread -C o -o $EK_FILE $EKCERT_NVHANDLE && [ ! -f $EK_FILE ]; then
  tpm2_getekcertificate -X -u $EK_PUB_FILE $EK_PORTAL | jq '.certificate' | sed -e 's/-/+/g' | sed -e 's/_/\//g' | sed -e 's/%3D/=/g' | sed -e 's/"//g' | base64 -d > $EK_FILE
fi

# Clean bad der format with spaces
openssl x509 -in $EK_FILE -inform der -outform der -out $EK_FILE

if ! tpm2_readpublic -c $AK_HANDLE; then
  tpm2_createak -C $EK_HANDLE -c $AK_CTX -G rsa -g sha256 -s rsassa -u $AK_PUB_FILE -n $AK_NAME_FILE > $AK_OUT
  # remove any previous AK loaded on the TPM
  set +e
  tpm2_evictcontrol -c $AK_HANDLE
  debugger
  
  # load ak into TPM nvram
  tpm2_evictcontrol -c $AK_CTX $AK_HANDLE
fi

tpm2_readpublic -c $AK_HANDLE -o $AK_TPM_PUB -f tpmt

AK_NAME=$(tpm2_readpublic -c $AK_HANDLE | grep ^name: | awk '{ print $2 }')
AK_PUB=$(cat $AK_TPM_PUB | base64 -w 0)
EK_CERT=$(cat $EK_FILE | base64 -w 0)
EK_PUB=$(cat $EK_TPM_PUB | base64 -w 0)

response=$(curl -L -k --request POST --url $ACA_URL/enroll/request/ --header 'content-type: application/json' --data "{ \"ak-pub\": \"${AK_PUB}\", \"ak-name\": \"${AK_NAME}\", \"ek-cert\": \"${EK_CERT}\", \"ek-pub\": \"${EK_PUB}\" }")

echo $response | jq '.certificate' -r | base64 -d > $CERTIFICATE_FILE_ENC
echo $response | jq '.credential' -r | base64 -d > $CREDENTIAL_FILE
IV=$(echo $response | jq '.iv' -r)

tpm2_startauthsession --policy-session -S $SESSION_FILE
tpm2_policysecret -S $SESSION_FILE -c e
tpm2_activatecredential -Q -c $AK_HANDLE -C $EK_HANDLE -i $CREDENTIAL_FILE -o $ACTIVATION_OUTPUT -P"session:$SESSION_FILE"
tpm2_flushcontext $SESSION_FILE

krypto d -in $CERTIFICATE_FILE_ENC -out $CERTIFICATE_FILE -iv $IV -k "$(cat $ACTIVATION_OUTPUT)"

set +e
tpm2_nvundefine -Q $AIK_NV -C o
debugger
tpm2_nvdefine -Q $AIK_NV -C o -a "ownerread|policywrite|ownerwrite|no_da|policyread|authread"

tpm2_nvwrite -Q $AIK_NV -C o -i $CERTIFICATE_FILE

tpm2_nvread -C o $AIK_NV | openssl x509 --inform der -text

if [ ! "$DEBUG" == true ]; then
  rm -rf $TMP_FOLDER;
fi

echo ""
echo "Certificate created and saved in handle $AIK_NV"