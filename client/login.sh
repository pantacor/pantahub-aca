#!/bin/bash

#
# This scripts get the JWT token using the IDevID certificate
#

source $(dirname $0)/const.sh
debugger

idevid_base64=`tpm2_nvread -C o $IDEVID_NV | openssl x509 -inform der -outform pem | base64 -w 0`
device_id=`tpm2_nvread -C o $IDEVID_NV | openssl x509 -inform der -noout -subject | awk -F= '{print $NF}' | sed -e 's/\ //g'`

response=$(curl \
  -H "Content-Type: application/json" \
	-X POST \
	-d '{"username": "prn:::devices:/'${device_id}'", "password" : "'${idevid_base64}'"}' \
	${PH_BASE_URL}/auth/login)

echo $response | jq -r '.token'