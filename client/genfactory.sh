#!/bin/bash

#
# This scripts generates a TPM backed IDevID.
# * create tpm rsa key pair (IDevId Object)
# * create self signed cert (IDevId Cert); this includes extensions:
#   - for OWNER PRN
#   - NAME SIG BY OWNER
#   - TPM2CERTIFY ATTEST AND SIG
#
# IDevId is only used for "rare" operation to get Service and SystemState
# specific ServiceDevId's issued and certified. Hence server side is expected
# to validate the provided extension values before issuing the appropriate
# cert on every request.
#
# This script is first run when you want the device to get a new identity
# that will be owned by OWNERPRN automatically/
#
# This script also takes a HEX encoded AUTOJOINTOKEN as input which needs
# to match AUTOJOINTOKEN in pantahub associated with the OWNERPRN account.
# This proofs that owner consented to allow this device into his account.
#
# Right now no transfer of ownership is implemented; new owners need to
# rerun this script which will reset all previously known identities
#
# After this script has run, device is expected to retrieve a valid AK Cert
# through enroll.sh first time when it comes online.
#
# Once AK Cert is retrieved, Local Service Certs can issued for a given
# system state through the /register endpoint (using the register script).
#

source $(dirname $0)/const.sh
debugger

OWNERPRN=$1
AUTOJOINTOKEN=$2
TOKEN_ID=$3

[ "$#" -eq 3 ] || die "3 argument required, $# provided. Args: OWNERPRN AUTOJOINTOKEN TOKEN_ID"

${NOINIT} || tpm2_clear

if ! tpm2_readpublic -c ${EK_HANDLE}; then
	tpm2_createek -c ${EK_HANDLE} -G rsa 
fi

tmpd=`mktemp -d genfactory-XXXXXXXXXX`

cd $tmpd
# create ak if not there yet
if ! tpm2_readpublic -c ${AK_HANDLE}; then
	tpm2_createak -C ${EK_HANDLE} -c ak.ctx -G rsa -g sha256 -s rsassa -u ak.pub -n ak.name
	tpm2_evictcontrol -c ${AK_HANDLE} || true
	tpm2_evictcontrol -c ak.ctx ${AK_HANDLE}
fi

# create idevid if not there yet
if ! tpm2_readpublic -c ${IDEVID_HANDLE}; then
	# create primary - no need to load
	tpm2_createprimary  -g sha256 -G rsa -c primary.ctx -d create.dig -t create.ticket

	# create the IdevId Key:
	tpm2_create -C primary.ctx -G rsa -u idevid.pub -r idevid.priv -a 'fixedtpm|fixedparent|sensitivedataorigin|userwithauth|decrypt|sign|noda'
	tpm2_load -u idevid.pub -r idevid.priv -n idevid.name -c idevid.ctx -C primary.ctx
	tpm2_evictcontrol -c idevid.ctx ${IDEVID_HANDLE}
fi

# issue idevid cert if not avail
# issue tpm2_certify for IDevIdKey
tpm2_certify -C ${IDEVID_HANDLE} \
	-c ${AK_HANDLE} \
	-g sha256 \
	-o idevid.tpm2certify.attest \
	-s idevid.tpm2certify.sig \
	-f tss
	
tpm2certify_attest_hex=`cat idevid.tpm2certify.attest | xxd -c 10000 -p | sed -e 's/\ //g'`
tpm2certify_sig_hex=`cat idevid.tpm2certify.sig | xxd -c 10000 -p | sed -e 's/\ //g'`

# we use 12 chars from the middle of the name hash as serial
SERIALID=`tpm2_readpublic -c ${AK_HANDLE} | grep ^name: | awk '{ print $2 }' | sed -e 's/..........//;s/\(........................\).*/\1/'`
OWNERHEX=`echo -n $OWNERPRN | xxd -c 10000 -p | sed -e 's/\ //g'`
TOKEN_ID_HEX=`echo -n $TOKEN_ID | xxd -c 10000 -p | sed -e 's/\ //g'`

token_sha=$(echo -n ${AUTOJOINTOKEN} | base64 -d | openssl dgst -sha256 | sed -e 's/.*= //')
name=$(tpm2_readpublic -c ${IDEVID_HANDLE} | grep ^name: | sed -e 's/.*://' | sed -e 's/ //g' | tr -d \\n | xxd -c 10000 -p | sed -e 's/\ //g')
sig=$(echo -n $name | openssl dgst -sha256 -mac hmac -macopt hexkey:`echo -n $token_sha` | sed -e 's/.*= //')

tpm2_readpublic -c $IDEVID_HANDLE -o idevid.pub -f tpmt
tpm2_readpublic -c $AK_HANDLE -o ak.pub -f tpmt

openssl req -new \
	-engine tpm2tss \
	-keyform engine \
	-key ${IDEVID_HANDLE} \
	-out idevid.csr \
	-outform PEM \
	-subj "/O=PantacorLtd/OU=PantahubDevices/CN=${SERIALID}@aca.pantahub.com/serialNumber=${SERIALID}/" \
	-addext ${PH_CERTIFY_ATTEST_OID}=DER:${tpm2certify_attest_hex} \
	-addext ${PH_CERTIFY_SIG_OID}=DER:${tpm2certify_sig_hex} \
	-addext ${PH_OWNERPRN_OID}=DER:${OWNERHEX} \
	-addext ${PH_HMAC_OID}=DER:${sig} \
	-addext ${PH_TOKEN_ID_OID}=DER:${TOKEN_ID_HEX} 

tpm2_nvundefine -Q $IDEVID_NV -C o || true
tpm2_nvdefine -Q $IDEVID_NV -C o -a "ownerread|policywrite|ownerwrite|no_da|policyread|authread"
tpm2_nvwrite -Q $IDEVID_NV -C o -i idevid.csr

cd -
if [ ! "$DEBUG" == true ]; then
  rm -r $tmpd
fi

echo "Factory state generation is DONE!"
echo "EK, AK and IDevID csr are created"
