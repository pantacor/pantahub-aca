#!/bin/bash

#
# This script can only run after genidevid.sh as well as enroll.sh
# scripts have been run.
#
# It will generate a new LDevId that is bound to current system state
# through policy and that is issued for use with a pantahub registered
# service.
#
# It is expected that these certs get issued on every new device revision
# and if lost they can be re-requested any time as long as the idevid
# is still valid.
#
# The main service is of course "api", but other services such as "cmp"
# also expose a /register endpoint that will work with this script.
# It is the device makers responsibility to store the LDevId keys and
# certificates in a way such that right clients on a device can
# make use of it.
#
# Register endpoints use TLS Client auth with IDevId to assert the clients
# authentity, ownership and integrity based on TPM measurements.
#

source $(dirname $0)/const.sh
debugger

tmpd=`mktemp -d register-XXXXXXXXXX`
cd $tmpd

# XXX: ensure we use policy in the end
#tpm2_createpolicy -L pcr.policy -g sha256 -l ${PCRLIST} --policy-pcr

#tpm2_create -L pcr.policy -g sha256 -G rsa \
# pol_hex=`cat pcr.policy | xxd -c 10000 -p | sed -e 's/\ //g'`

# create ldevid if not there yet
if ! tpm2_readpublic -c ${LDEVID_HANDLE}; then
	tpm2_createprimary -g sha256 -G rsa -c primary.ctx -d create.dig -t create.ticket

	tpm2_create -g sha256 -G rsa \
		-u ldevid.pub \
		-r ldevid.priv \
		-C primary.ctx \
		-a 'fixedtpm|fixedparent|sensitivedataorigin|userwithauth|decrypt|sign|noda'

	tpm2_load -u ldevid.pub -r ldevid.priv -n ldevid.name -c ldevid.ctx -C primary.ctx                                                         

	tpm2_evictcontrol -c ${LDEVID_HANDLE} || true
	tpm2_evictcontrol -c ldevid.ctx ${LDEVID_HANDLE}
fi


# certify that this key is loaded in TPM with given attributes
# implicitely certify the pcr policy that is matching
tpm2_certify -C ${LDEVID_HANDLE} \
	-c ${AK_HANDLE} \
	-g sha256 \
	-o ldevid.tpm2certify.attest \
	-s ldevid.tpm2certify.sig \
	-f tss
	
tpm2certify_attest_hex=`cat ldevid.tpm2certify.attest | xxd -c 10000 -p | sed -e 's/\ //g'`
tpm2certify_sig_hex=`cat ldevid.tpm2certify.sig | xxd -c 10000 -p | sed -e 's/\ //g'`

# TPM QUOTE
tpm2_quote -c ${AK_HANDLE} -l ${PCRLIST} -g sha256 -m quote.attest -s quote.sig -o quote.pcr -f tss

quote_attest_hex=`cat quote.attest | xxd -c 10000 -p | sed -e 's/\ //g'`
quote_sig_hex=`cat quote.sig | xxd -c 10000 -p | sed -e 's/\ //g'`
quote_pcr_hex=`echo ${PCRLIST} | xxd -c 10000 -p | sed -e 's/\ //g'`

SERIALID=`tpm2_readpublic -c ${AK_HANDLE} | grep ^name: | awk '{ print $2 }' | sed -e 's/..........//;s/\(............\).*/\1/'`

# now lets generate CSR that includes the above.
openssl req -x509 -new -engine tpm2tss \
	-days 1825 \
	-keyform engine --key ${LDEVID_HANDLE} \
	-out ldevid.csr \
	-outform der \
	-subj "/O=PantacorLtd/OU=PantahubDevices/CN=${SERIALID}.aca.pantahub.com@cmp.pantahub.com/serialNumber=${SERIALID}/" \
	-addext ${PH_CERTIFY_SIG_OID}=DER:${tpm2certify_sig_hex} \
	-addext ${PH_CERTIFY_ATTEST_OID}=DER:${tpm2certify_attest_hex} \
	-addext ${PH_QUOTE_ATTEST_OID}=DER:${quote_attest_hex} \
	-addext ${PH_QUOTE_SIG_OID}=DER:${quote_sig_hex} \
	-addext ${PH_QUOTE_PCR_OID}=DER:${quote_pcr_hex} \
	${NULL}

tpm2_nvread -C o $IDEVID_NV | openssl x509 -inform der -out idevid.crt -outform pem
tpm2_nvread -C o $AIK_NV | openssl x509 -inform der -out ak.crt -outform pem
tpm2_readpublic -c $IDEVID_HANDLE -o idevid.pub -f tpmt
tpm2_readpublic -c $AK_HANDLE -o ak.pub -f tpmt
tpm2_readpublic -c $LDEVID_HANDLE -o lidevid.pub -f tpmt

akcert_hex=`cat ak.crt | xxd -c 10000 -p | sed -e 's/\ //g'`
akpub_hex=`cat ak.pub | xxd -c 10000 -p | sed -e 's/\ //g'`
idevidpub_hex=`cat idevid.pub | xxd -c 10000 -p | sed -e 's/\ //g'`
# idevidcert_hex=`cat idevid.crt | xxd -c 10000 -p | sed -e 's/\ //g'`

csr_base64=`cat ldevid.csr | base64 -w 0`
ldevpub_base64=`cat lidevid.pub | base64 -w 0`

# SERVICE/register will validate
#  * ak-cert provided is valid and is signed by trusted CA (e.g. aca.pantahub.com)
#  * ldevid.csr has the critical fields in subj and esxtensions
#  * idevid and ldevid are certified through the (same) ak-cert
#  * idevid owner is a pantahub user who has signed up for the SERVICE
#  * ldevid certify attestation data is matching the ldev-pub and the public RSA
#    key of the CSR
# 
# on success of validation, the SERVICE will:
#   * facilitate hat the CSR gets signed
#   * potentially add extensions that will help the authorization and resource allocation
#     for the backend service
#   * potentially issue new subj and subjAltName extension as needed by service;
#     original values will be remembered in separate extensions
#
# example: POST cmp.pantahub.com/register/sgw.pantahub.com/
# example: POST cmp.pantahub.com/register/api.pantahub.com/
#
# -H "PhProxyTlsToken: abcde1234" \
# -H "PhClientCertificate: $idevidcert_hex" \

curl --engine tpm2tss \
	-k \
	--key-type ENG \
	--cert-type PEM \
	--key $IDEVID_HANDLE \
	--cert idevid.crt \
	-H "Content-Type: application/json" \
	-H "PhTpm2ClientIDevPub:${idevidpub_hex}" \
	-H "PhTpm2ClientIAKCert:${akcert_hex}" \
	-H "PhTpm2ClientIAKPub:${akpub_hex}" \
	-X POST \
	-d '{
		"csr" : "'${csr_base64}'",
		"ldev-pub": "'${ldevpub_base64}'",
		"pcr-selection": "'${PCRLIST}'"
	}' \
	${ACA_URL}/register/pantahub > result.json

cat result.json | jq -r '.crt' | base64 -di | openssl x509 -inform der -out ldevid.crt --outform der

tpm2_nvundefine -Q $LDEVID_NV -C o || true
if ! tpm2_nvdefine -Q $LDEVID_NV -C o -a "ownerread|policywrite|ownerwrite|no_da|policyread|authread"; then
	mv ldevid.crt ../
	cat ../ldevid.crt | openssl x509 -inform der -text 
else
	tpm2_nvwrite -Q $LDEVID_NV -C o -i ldevid.crt
	tpm2_nvread -C o $LDEVID_NV | openssl x509 -inform der -text 
fi

cd -
rm -r $tmpd

# Result will provide the fully signed PEM and CRT (complete chain of trust).

