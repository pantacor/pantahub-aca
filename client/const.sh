#!/bin/bash
die () {
  echo >&2 "$@"
  exit 1
}

debugger () {
  if [ "$DEBUG" == true ]; then set -ex; else set -e; fi
}

NOINIT=${NOINIT:-true}

PEN_OID=1.3.6.1.4.1                                              
PH_OID=${PEN_OID}.54621                                                            
PH_OWNERPRN_OID=${PH_OID}.100.1
PH_HMAC_OID=${PH_OID}.100.2
PH_TOKEN_ID_OID=${PH_OID}.100.3
PH_CERTIFY_ATTEST_OID=${PH_OID}.100.4
PH_CERTIFY_SIG_OID=${PH_OID}.100.5
PH_QUOTE_ATTEST_OID=${PH_OID}.100.6
PH_QUOTE_SIG_OID=${PH_OID}.100.7
PH_QUOTE_PCR_OID=${PH_OID}.100.8
PH_DEVICE_PRN_OID=${PH_OID}.100.9

EK_CERT_HANDLE=0x1c00002
AIK_NV=0x1c00003
IDEVID_NV=0x01C90000
LDEVID_NV=0x01C90001
EK_HANDLE=0x81010001
AK_HANDLE=0x81010002
IDEVID_HANDLE=0x81020001
LDEVID_HANDLE=0x81020002

PCRLIST=${PCRLIST:-sha256:0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}
ACA_URL=${ACA_URL:-https://api.aca.pantahub.com}
EK_PORTAL=${EK_PORTAL:-https://ekop.intel.com/ekcertservice/}
PH_BASE_URL=${PH_BASE_URL:-https://api.pantahub.com}