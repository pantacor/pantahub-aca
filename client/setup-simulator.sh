#!/bin/bash

source $(dirname $0)//const.sh
debugger

if [ -z "$1" ]; then
  TMP_FOLDER=$(mktemp -d /tmp/setup.XXXXXX)
else
  TMP_FOLDER=$1
fi

tpm2_createek -c $TMP_FOLDER/ek.handle -G rsa -f pem -u $TMP_FOLDER/ek.pub.pem

pki --gen --type ecdsa --size 256 --outform pem > $TMP_FOLDER/tpmVendorKey.pem

pki --self --ca --type ecdsa --in $TMP_FOLDER/tpmVendorKey.pem \
	--dn="C=US, O=Pantahub Demo, CN=Pantahub Demo ACA" \
	--lifetime 3652 --outform pem > $TMP_FOLDER/tpmVendorCert.pem

serial=simdev-`date +%s`

pki --issue --cacert $TMP_FOLDER/tpmVendorCert.pem --cakey $TMP_FOLDER/tpmVendorKey.pem --type pub \
	--in $TMP_FOLDER/ek.pub.pem --dn "C=UK, O=Pantahub Demo, OU=EKs RSA, CN=${serial}.tpms.aca.pantahub.com" \
	--lifetime 3651 --outform der > $TMP_FOLDER/ek-cert.der

set +e
tpm2_nvundefine -Q $EK_CERT_HANDLE -C p
set -e

tpm2_nvdefine -Q $EK_CERT_HANDLE -C p -a "ownerread|policywrite|ownerwrite|platformcreate|no_da|policyread|authread"

tpm2_nvwrite -Q $EK_CERT_HANDLE -C o -i $TMP_FOLDER/ek-cert.der

# This doesn't work on TPM2_TOOLS v4.0
tpm2_nvreadpublic
