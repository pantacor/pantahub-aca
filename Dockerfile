FROM golang:alpine as builder

ENV GO111MODULE=on

RUN apk add -U git
COPY . /src/gitlab.com/pantacor/pantahub-aca
WORKDIR /src/gitlab.com/pantacor/pantahub-aca
RUN go install -v ./...

FROM alpine:3.10

COPY ./client/asac-5d72164a.rsa.pub /etc/apk/keys/asac-5d72164a.rsa.pub

RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/main' >> /etc/apk/repositories \
    && echo 'http://dl-cdn.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories \
    && echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories \
    && echo 'https://aports.s3.amazonaws.com/next/testing' >> /etc/apk/repositories

# install depencies
RUN apk add --no-cache \
    autoconf \
    autoconf-archive \
    automake \
    binutils \
    build-base \
    ca-certificates \
    cmocka-dev \
    coreutils \
    curl \
    curl-dev \
    dbus-x11 \
    doxygen \
    findutils \
    git \
    glib-dev \
    grep \
    iproute2 \
    jq \
    libltdl \
    libtool \
    linux-headers \
    openssl \
    openssl-dev \
    pkgconf \
    procps \
    procps-dev \
    strongswan \
    uthash-dev \
    tpm2-tools \
    ibm-tpm2-simulator \
    tpm2-tss \
    tpm2-tss-tcti-mssim \
    tpm2-tss-esys \
    tpm2-tss-tctildr \
    tpm2-tss-rc \
    tpm2-tss-sys \
    tpm2-tss-mu \
    tpm2-tss-tcti-device \
    tpm2-tss-engine \
    wait4ports \
    && rm -rf /var/cache/apk/*
 
# install openrc
RUN apk add --no-cache openrc su-exec ca-certificates wait4ports \
    # Disable getty's
    && sed -i 's/^\(tty\d\:\:\)/#\1/g' /etc/inittab \
    && sed -i \
        # Change subsystem type to "docker"
        -e 's/#rc_sys=".*"/rc_sys="docker"/g' \
        # Allow all variables through
        -e 's/#rc_env_allow=".*"/rc_env_allow="\*"/g' \
        # Start crashed services
        -e 's/#rc_crashed_stop=.*/rc_crashed_stop=NO/g' \
        -e 's/#rc_crashed_start=.*/rc_crashed_start=YES/g' \
        # Define extra dependencies for services
        -e 's/#rc_provide=".*"/rc_provide="loopback net"/g' \
        /etc/rc.conf \
    # Remove unnecessary services
    && rm -f /etc/init.d/hwdrivers \
            /etc/init.d/hwclock \
            /etc/init.d/hwdrivers \
            /etc/init.d/modules \
            /etc/init.d/modules-load \
            /etc/init.d/modloop \
    # Can't do cgroups
    && sed -i 's/\tcgroup_add_service/\t#cgroup_add_service/g' /lib/rc/sh/openrc-run.sh \
    && sed -i 's/VSERVER/DOCKER/Ig' /lib/rc/sh/init.sh

# create tpm2 user
RUN adduser -S tss

WORKDIR /tmp
COPY --from=builder /go/bin/pantahub-aca /usr/local/bin/pantahub-aca

# install services
RUN dbus-daemon --system
# COPY ./docker_confs/services/pantahub-aca /etc/init.d/
COPY ./docker_confs/services/tpm_server /etc/init.d/

# enable default services
RUN rc-update add tpm_server default
# RUN rc-update add pantahub-aca default

CMD ["/sbin/init"]
