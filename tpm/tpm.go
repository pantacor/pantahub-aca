// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package tpm

import (
	"os/exec"
)

const (
	// CmdActivatecredential command
	CmdActivatecredential = "activatecredential"
	// CmdCertify command
	CmdCertify = "tpm2_certify"
	// CmdChangeauth command
	CmdChangeauth = "tpm2_changeauth"
	// CmdCheckquote command
	CmdCheckquote = "tpm2_checkquote"
	// CmdClear command
	CmdClear = "tpm2_clear"
	// CmdClearcontrol command
	CmdClearcontrol = "tpm2_clearcontrol"
	// CmdCreate command
	CmdCreate = "tpm2_create"
	// CmdCreateak command
	CmdCreateak = "tpm2_createak"
	// CmdCreateek command
	CmdCreateek = "tpm2_createek"
	// CmdCreatepolicy command
	CmdCreatepolicy = "tpm2_createpolicy"
	// CmdCreateprimary command
	CmdCreateprimary = "tpm2_createprimary"
	// CmdDictionarylockout command
	CmdDictionarylockout = "tpm2_dictionarylockout"
	// CmdDuplicate command
	CmdDuplicate = "tpm2_duplicate"
	// CmdEncryptdecrypt command
	CmdEncryptdecrypt = "tpm2_encryptdecrypt"
	// CmdEvictcontrol command
	CmdEvictcontrol = "tpm2_evictcontrol"
	// CmdFlushcontext command
	CmdFlushcontext = "tpm2_flushcontext"
	// CmdGetcap command
	CmdGetcap = "tpm2_getcap"
	// CmdGetekcertificate command
	CmdGetekcertificate = "tpm2_getekcertificate"
	// CmdGetrandom command
	CmdGetrandom = "tpm2_getrandom"
	// CmdGettestresult command
	CmdGettestresult = "tpm2_gettestresult"
	// CmdHash command
	CmdHash = "tpm2_hash"
	// CmdHierarchycontrol command
	CmdHierarchycontrol = "tpm2_hierarchycontrol"
	// CmdHmac command
	CmdHmac = "tpm2_hmac"
	// CmdImport command
	CmdImport = "tpm2_import"
	// CmdIncrementalselftest command
	CmdIncrementalselftest = "tpm2_incrementalselftest"
	// CmdLoad command
	CmdLoad = "tpm2_load"
	// CmdLoadexternal command
	CmdLoadexternal = "tpm2_loadexternal"
	// CmdMakecredential command
	CmdMakecredential = "tpm2_makecredential"
	// CmdNvdefine command
	CmdNvdefine = "tpm2_nvdefine"
	// CmdNvincrement command
	CmdNvincrement = "tpm2_nvincrement"
	// CmdNvread command
	CmdNvread = "tpm2_nvread"
	// CmdNvreadlock command
	CmdNvreadlock = "tpm2_nvreadlock"
	// CmdNvreadpublic command
	CmdNvreadpublic = "tpm2_nvreadpublic"
	// CmdNvundefine command
	CmdNvundefine = "tpm2_nvundefine"
	// CmdNvwrite command
	CmdNvwrite = "tpm2_nvwrite"
	// CmdPcrallocate command
	CmdPcrallocate = "tpm2_pcrallocate"
	// CmdPcrevent command
	CmdPcrevent = "tpm2_pcrevent"
	// CmdPcrextend command
	CmdPcrextend = "tpm2_pcrextend"
	// CmdPcrread command
	CmdPcrread = "tpm2_pcrread"
	// CmdPcrreset command
	CmdPcrreset = "tpm2_pcrreset"
	// CmdPolicyauthorize command
	CmdPolicyauthorize = "tpm2_policyauthorize"
	// CmdPolicyauthorizenv command
	CmdPolicyauthorizenv = "tpm2_policyauthorizenv"
	// CmdPolicycommandcode command
	CmdPolicycommandcode = "tpm2_policycommandcode"
	// CmdPolicyduplicationselect command
	CmdPolicyduplicationselect = "tpm2_policyduplicationselect"
	// CmdPolicylocality command
	CmdPolicylocality = "tpm2_policylocality"
	// CmdPolicyor command
	CmdPolicyor = "tpm2_policyor"
	// CmdPolicypassword command
	CmdPolicypassword = "tpm2_policypassword"
	// CmdPolicypcr command
	CmdPolicypcr = "tpm2_policypcr"
	// CmdPolicyrestart command
	CmdPolicyrestart = "tpm2_policyrestart"
	// CmdPolicysecret command
	CmdPolicysecret = "tpm2_policysecret"
	// CmdPrint command
	CmdPrint = "tpm2_print"
	// CmdQuote command
	CmdQuote = "tpm2_quote"
	// CmdRcDecode command
	CmdRcDecode = "tpm2_rc_decode"
	// CmdReadclock command
	CmdReadclock = "tpm2_readclock"
	// CmdReadpublic command
	CmdReadpublic = "tpm2_readpublic"
	// CmdRsadecrypt command
	CmdRsadecrypt = "tpm2_rsadecrypt"
	// CmdRsaencrypt command
	CmdRsaencrypt = "tpm2_rsaencrypt"
	// CmdSelftest command
	CmdSelftest = "tpm2_selftest"
	// CmdSend command
	CmdSend = "tpm2_send"
	// CmdShutdown command
	CmdShutdown = "tpm2_shutdown"
	// CmdSign command
	CmdSign = "tpm2_sign"
	// CmdStartauthsession command
	CmdStartauthsession = "tpm2_startauthsession"
	// CmdStartup command
	CmdStartup = "tpm2_startup"
	// CmdStirrandom command
	CmdStirrandom = "tpm2_stirrandom"
	// CmdTestparms command
	CmdTestparms = "tpm2_testparms"
	// CmdUnseal command
	CmdUnseal = "tpm2_unseal"
	// CmdVerifysignature command
	CmdVerifysignature = "tpm2_verifysignature"
)

// Run runs a tpm2 command
func Run(command string, params ...string) ([]byte, error) {
	cmd := exec.Command(command, params...)
	response, err := cmd.CombinedOutput()

	return response, err
}
