// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package models

// EnvDefaults : EnvDefaults Structure to hold Env default Variable values
type EnvDefaults struct {
	// ServerHost Go server host and port
	ServerHost string

	// Pantahub CA
	CaCert string

	// AcaCertificate ACA certificate in der format and base64 encoded
	AcaCertificate string

	// AcaPrivateKey ACA private key in pem format and base64 encoded
	AcaPrivateKey string

	// APIHost server host
	APIHost string

	// APIScheme server scheme
	APIScheme string

	// ServiceName service name
	ServiceName string

	// TrustedRoots array of trusted root certificates
	TrustedRoots []string

	// TrustedRootsBundle array of trusted root certificates
	TrustedRootsBundle string

	// ProxyTLSUnlockAuthToken bearer token
	ProxyTLSUnlockAuthToken string

	// BaseApiHost base api host and port
	BaseAPIHost string

	// BaseAPIScheme api scheme could be http or https
	BaseAPIScheme string
}
