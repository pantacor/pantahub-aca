// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package models

import "encoding/asn1"

// PhCertExtensions all the indentifiers for pantahub extensions on a certificate struct
type PhCertExtensions struct {
	AIKName       asn1.ObjectIdentifier
	OwnerPrnOID   asn1.ObjectIdentifier
	OwnernameSig  asn1.ObjectIdentifier
	TokenID       asn1.ObjectIdentifier
	CertifyAttest asn1.ObjectIdentifier
	CertifySig    asn1.ObjectIdentifier
	QuoteAttest   asn1.ObjectIdentifier
	QuoteSig      asn1.ObjectIdentifier
	QuotePcrList  asn1.ObjectIdentifier
	DevicePRN     asn1.ObjectIdentifier
}
