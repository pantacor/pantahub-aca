// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

// CertType type of certificate
type CertType string

const (
	// CertsCollectionName mongo collection name
	CertsCollectionName = "certs"

	// AIKType certificate type IAK
	AIKType CertType = "AIK"

	// LDevIDType certificate type
	LDevIDType CertType = "LDEVID"
)

// Cert save accredited certs
type Cert struct {
	ID        *bson.ObjectId `json:"id" bson:"_id" readOnly:"true" optional:"true" description:"Certificate identifier"`
	PRN       string         `json:"prn" readOnly:"true" optional:"true" description:"Register PRN"`
	Cert      string         `json:"crt" bson:"crt" description:"cert content"`
	Type      CertType       `json:"type" bson:"type" description:"type of certificate"`
	DeletedAt *time.Time     `json:"-" bson:"deleted_at" optional:"true"`
	UpdatedAt *time.Time     `json:"updated_at" bson:"updated_at" optional:"true"`
	CreatedAt *time.Time     `json:"created_at" bson:"created_at"`
}
