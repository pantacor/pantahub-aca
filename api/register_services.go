// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package api

import (
	"gitlab.com/pantacor/pantahub-aca/api/certs"
	"gitlab.com/pantacor/pantahub-aca/api/enroll"
	"gitlab.com/pantacor/pantahub-aca/api/health"
	"gitlab.com/pantacor/pantahub-aca/api/metrics"
	"gitlab.com/pantacor/pantahub-aca/api/register"

	"github.com/emicklei/go-restful"
)

func (s *server) registerServices() {
	restful.Add(enroll.NewService())
	restful.Add(health.NewService())
	restful.Add(metrics.NewService())
	restful.Add(certs.NewService())
	restful.Add(register.NewService())
}
