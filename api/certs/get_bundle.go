// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package certs

import (
	"encoding/base64"
	"encoding/pem"
	"errors"
	"log"
	"net/http"
	"regexp"

	"github.com/emicklei/go-restful"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/pantacor/pantahub-aca/storage"
	"gitlab.com/pantacor/pantahub-aca/utils"
)

// GetBundleCertificate return a certificate from a given ID
func GetBundleCertificate(request *restful.Request, response *restful.Response) {
	defer func() {
		if r := recover(); r != nil {
			log.Println(r)
			utils.ErrorResponse(response, errors.New("Cert not found"), http.StatusBadRequest, GetBachErrorCodes.NotFound)
		}
	}()

	acaCertificate, err := base64.StdEncoding.DecodeString(utils.Env.AcaCertificate)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, GetBachErrorCodes.ACACertNotFound)
		return
	}

	parseID := request.PathParameter("cert-id")
	getCertID := regexp.MustCompile(`([\d\w]*)\.?(.*)`)
	match := getCertID.FindStringSubmatch(parseID)
	id := string(match[1])
	st := storage.NewStorage()

	enroll, err := st.FindCert(bson.ObjectIdHex(id))
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, GetBachErrorCodes.NotFound)
		return
	}

	certDer, err := base64.StdEncoding.DecodeString(enroll.Cert)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, GetBachErrorCodes.DecodingCert)
		return
	}

	cert := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: certDer})
	aca := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: acaCertificate})

	full := append(aca, cert...)
	response.Write(full)
}
