// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package certs

import (
	"net/http"

	"github.com/emicklei/go-restful"
	restfulspec "github.com/emicklei/go-restful-openapi"
)

const (
	// GetURL endpoint URL
	GetURL = "/{cert-id}"

	// GetBundleURL endpoint URL
	GetBundleURL = "/{cert-id}/bundle"

	// ServiceURL base endpoint URL
	ServiceURL = "/certs"
)

// NewService create new certs service
func NewService() *restful.WebService {
	tags := []string{"certs"}
	ws := new(restful.WebService)

	ws.Path(ServiceURL).
		Produces("text/plain")

	ws.Route(ws.GET(GetURL).To(GetCertificate).
		Doc("Get a AIK certificate from a given ID").
		Param(ws.PathParameter("cert-id", "identifier of the certificate").DataType("string")).
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Operation("GetCertificate").
		Writes([]string{}).
		Returns(http.StatusOK, "OK", []string{""}))

	ws.Route(ws.GET(GetBundleURL).To(GetBundleCertificate).
		Doc("Get a AIK certificate from a given ID").
		Param(ws.PathParameter("cert-id", "identifier of the certificate").DataType("string")).
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Operation("GetBundleCertificate").
		Writes([]string{}).
		Returns(http.StatusOK, "OK", []string{""}))

	return ws
}
