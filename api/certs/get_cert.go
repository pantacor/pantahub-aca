// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package certs

import (
	"errors"
	"log"
	"net/http"
	"regexp"

	"github.com/emicklei/go-restful"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/pantacor/pantahub-aca/storage"
	"gitlab.com/pantacor/pantahub-aca/utils"
)

// PantaACARoot certificate name
const PantaACARoot = "pantaACARoot"

// GetCertificate return a certificate from a given ID
func GetCertificate(request *restful.Request, response *restful.Response) {
	defer func() {
		if r := recover(); r != nil {
			log.Println(r)
			utils.ErrorResponse(response, errors.New("Cert not found"), http.StatusBadRequest, GetErrorCodes.NotFound)
		}
	}()

	parseID := request.PathParameter("cert-id")
	getCertID := regexp.MustCompile(`([\d\w]*)\.?(.*)`)
	match := getCertID.FindStringSubmatch(parseID)
	id := string(match[1])
	format := string(match[2])

	var certBase64 string

	//  If id is pantaACARoot we should return the ACA certificate from environment
	if id == PantaACARoot {
		certBase64 = utils.Env.AcaCertificate
	} else { // In any other case look for the certificate ID on data base
		st := storage.NewStorage()

		enroll, err := st.FindCert(bson.ObjectIdHex(id))
		if err != nil {
			utils.ErrorResponse(response, err, http.StatusBadRequest, GetErrorCodes.NotFound)
			return
		}

		certBase64 = enroll.Cert
	}

	cert, err := utils.FromBase64DerToExtension(certBase64, format)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, GetErrorCodes.DecodingCert)
		return
	}

	response.Write(cert)
}
