// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package api

import (
	"net/http"

	"gitlab.com/pantacor/pantahub-aca/api/filters"
	"gitlab.com/pantacor/pantahub-aca/storage"
	"gitlab.com/pantacor/pantahub-aca/utils"

	"github.com/emicklei/go-restful"
	restfulspec "github.com/emicklei/go-restful-openapi"
	"github.com/go-openapi/spec"
	log "github.com/sirupsen/logrus"
)

// Server Application server
type Server interface {
	Run(address string)
}

type server struct{}

func (s *server) setSwaggerMetadata(swo *spec.Swagger) {
	swo.Info = &spec.Info{
		InfoProps: spec.InfoProps{
			Title:       "Pantacor ACA",
			Description: "",
			Contact: &spec.ContactInfo{
				ContactInfoProps: spec.ContactInfoProps{
					Name:  "Contact",
					Email: "contact@pantacor.com",
					URL:   "https://aca.pantacor.com",
				},
			},
			Version: "1.0.0",
		},
	}

	// supported tags
	swo.Tags = []spec.Tag{{TagProps: spec.TagProps{
		Name:        "ACA",
		Description: "Managing certificates"}}}

	// security schema
	swo.SecurityDefinitions = map[string]*spec.SecurityScheme{
		"jwt": spec.APIKeyAuth("Authorization", "header"),
	}
}

func (s *server) setupCORS() {
	cors := restful.CrossOriginResourceSharing{
		AllowedHeaders: []string{"Content-Type", "Accept"},
		AllowedMethods: []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowedDomains: []string{"*"},
		CookiesAllowed: false,
		Container:      restful.DefaultContainer,
	}
	restful.DefaultContainer.Filter(cors.Filter)
	restful.DefaultContainer.Filter(filters.OptionsFilters)
}

func (s *server) setupSwagger() {
	config := restfulspec.Config{
		WebServices:                   restful.RegisteredWebServices(),
		APIPath:                       "/swagger.json",
		PostBuildSwaggerObjectHandler: s.setSwaggerMetadata,
	}

	restful.DefaultContainer.Add(restfulspec.NewOpenAPIService(config))
}

func (s *server) setup() {
	// faster router
	restful.DefaultContainer.Router(restful.CurlyRouter{})

	// Add Global Filters to the server
	restful.Filter(filters.GlobalLogging)
	restful.Filter(filters.ResponseCodeLog)
	restful.Filter(filters.MeasureTimeLog)
	restful.Filter(filters.MetricsFilter)

	// set default mime-type
	restful.DefaultRequestContentType(restful.MIME_JSON)
	restful.DefaultResponseContentType(restful.MIME_JSON)

	// restful.EnableTracing(true)

	// register all services
	s.registerServices()

	// cors
	s.setupCORS()

	// swagger
	s.setupSwagger()

	// env default values
	utils.SetupEnvDefaults()

	// Setup storage demo data
	storage.NewStorage()
}

func (s *server) Run(address string) {
	s.setup()

	log.Infof("Running server at %s", address)
	log.Fatal(http.ListenAndServe(address, nil))
}

// NewServer return new server instance
func NewServer() Server {
	return &server{}
}
