// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package enroll

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/base64"
	"encoding/binary"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"math/big"
	"net/url"
	"os"
	"time"

	"github.com/google/go-tpm/tpm2"
	"gitlab.com/pantacor/pantahub-aca/api/certs"
	"gitlab.com/pantacor/pantahub-aca/tpm"
	"gitlab.com/pantacor/pantahub-aca/utils"
)

const (
	secretPhraseSize = 32
	ivSize           = 8
	akDefaultAttrs   = 327794
)

var extSubjectAltNameID = asn1.ObjectIdentifier{2, 5, 29, 17}
var authorityInformationAccess = asn1.ObjectIdentifier{1, 3, 6, 1, 5, 5, 7, 1, 1}

func verifyEk(ek *x509.Certificate) error {
	roots, err := utils.GetTrustedRootsFromBundle()
	if err != nil {
		return err
	}

	ek.UnhandledCriticalExtensions = []asn1.ObjectIdentifier{}

	opts := x509.VerifyOptions{
		Roots: roots,
		KeyUsages: []x509.ExtKeyUsage{
			x509.ExtKeyUsageAny,
		},
	}

	_, err = ek.Verify(opts)

	return err
}

func validateCertAndPub(cert *x509.Certificate, pub tpm2.Public) error {
	pubKey, err := pub.Key()
	if err != nil {
		return err
	}

	if fmt.Sprintf("%s", pubKey) != fmt.Sprintf("%s", cert.PublicKey) {
		return errors.New("Ek certificate public key is diferent to the ek public")
	}

	return nil
}

func validateAkPublic(akPub tpm2.Public, name string) error {
	if akPub.Attributes != akDefaultAttrs {
		return errors.New(RequestEnrollErrors.AKInvalid)
	}

	akNameString, err := utils.Tpm2PublicName(akPub)
	if err != nil {
		return err
	}

	if akNameString != name {
		return errors.New(RequestEnrollErrors.AKInvalid)
	}

	_, err = akPub.Key()
	if err != nil {
		return err
	}

	return nil
}

func createCertificate(akPublic crypto.PublicKey, certID, akName string) ([]byte, error) {
	acaCertificate, err := base64.StdEncoding.DecodeString(utils.Env.AcaCertificate)
	if err != nil {
		return nil, err
	}

	acaPrivate, err := base64.StdEncoding.DecodeString(utils.Env.AcaPrivateKey)
	if err != nil {
		return nil, err
	}

	acaCert, err := x509.ParseCertificate(acaCertificate)
	if err != nil {
		return nil, err
	}

	pemPrivateBlock, _ := pem.Decode(acaPrivate)
	if pemPrivateBlock == nil {
		return nil, errors.New("Aca private key missing")
	}

	privateKey, err := utils.ParsePrivateKey(pemPrivateBlock.Bytes)
	if err != nil {
		return nil, err
	}

	certURL, err := certs.BuildCertURL(certID)
	if err != nil {
		return nil, err
	}

	serialNumber := new(big.Int)
	serialNumber.SetBytes([]byte(certID))
	sn := akName[10:34]

	certTemplate := x509.Certificate{
		Version:      3,
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization:       []string{"Pantahub Ltd"},
			OrganizationalUnit: []string{"PantahubDevices"},
			CommonName:         "aca.pantahub.com",
			SerialNumber:       sn,
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().Add(time.Hour * 24 * 365),
		KeyUsage:  x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage: []x509.ExtKeyUsage{
			x509.ExtKeyUsageServerAuth,
		},
		ExtraExtensions: []pkix.Extension{
			pkix.Extension{
				Id:    utils.PhCertExtensionIDs.AIKName,
				Value: []byte(akName),
			},
		},
		URIs:                  []*url.URL{certURL},
		IsCA:                  false,
		BasicConstraintsValid: true,
	}

	certificate, err := x509.CreateCertificate(rand.Reader, &certTemplate, acaCert, akPublic, privateKey)
	if err != nil {
		return nil, err
	}

	return certificate, nil
}

func createCredentials(enrollData *erDecoded) ([]byte, []byte, error) {
	ekPub, err := utils.CreateTmpFile(enrollData.EkPubBin, "ekPub", "")
	if err != nil {
		return nil, nil, err
	}
	defer os.Remove(ekPub) // clean up

	encryptedSecret0 := make([]byte, secretPhraseSize)
	_, err = rand.Read(encryptedSecret0)
	if err != nil {
		return nil, nil, err
	}

	encryptedSecret := []byte(hex.EncodeToString(encryptedSecret0))[0:secretPhraseSize]

	secret, err := utils.CreateTmpFile(encryptedSecret, "secret", "")
	if err != nil {
		return nil, nil, err
	}
	defer os.Remove(secret) // clean up

	cert, err := utils.CreateTmpFile(nil, "cert", "")
	if err != nil {
		return nil, nil, err
	}
	defer os.Remove(cert) // clean up

	r, err := tpm.Run(
		tpm.CmdMakecredential,
		"-e"+ekPub,
		"-n"+enrollData.AkName,
		"-s"+secret,
		"-o"+cert,
	)
	if err != nil {
		return nil, nil, errors.New(string(r))
	}

	data, err := ioutil.ReadFile(cert)
	if err != nil {
		return nil, nil, err
	}

	return data, encryptedSecret, nil
}

func parseRsaPrivateKeyFromPemStr(privPEM string) (*crypto.PrivateKey, error) {
	block, _ := pem.Decode([]byte(privPEM))
	if block == nil {
		return nil, errors.New("failed to parse PEM block containing the key")
	}

	priv, err := utils.ParsePrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	return &priv, nil
}

func decodeEnrollRequest(er *EnrollmentRequest) (*erDecoded, error) {
	ekCertDer, err := base64.StdEncoding.DecodeString(er.EkCert)
	if err != nil {
		return nil, err
	}

	ekPubDer, err := base64.StdEncoding.DecodeString(er.EkPub)
	if err != nil {
		return nil, err
	}

	akPubDer, err := base64.StdEncoding.DecodeString(er.AkPub)
	if err != nil {
		return nil, err
	}

	akPub, err := tpm2.DecodePublic(akPubDer)
	if err != nil {
		return nil, err
	}

	ekPub, err := tpm2.DecodePublic(ekPubDer)
	if err != nil {
		return nil, err
	}

	ekCert, err := x509.ParseCertificate(ekCertDer)
	if err != nil {
		return nil, err
	}

	akPubKey, err := akPub.Key()
	if err != nil {
		return nil, err
	}

	ekPubBin, err := convertTpmtToTpmb(ekPubDer)
	if err != nil {
		return nil, err
	}

	return &erDecoded{
		AkPub:    akPub,
		AkName:   er.AkName,
		EkCert:   ekCert,
		EkPub:    ekPub,
		EkPubBin: ekPubBin,
		AkPubKey: akPubKey,
	}, nil
}

func convertTpmtToTpmb(tpmt []byte) ([]byte, error) {
	size := new(bytes.Buffer)
	err := binary.Write(size, binary.BigEndian, uint16(len(tpmt)))
	if err != nil {
		return nil, err
	}
	tpmBPublic := append(size.Bytes(), tpmt...)

	return tpmBPublic, nil
}

func encrypt(text, key []byte) ([]byte, []byte, error) {
	ivRandom := make([]byte, ivSize)
	iv := make([]byte, hex.EncodedLen(ivSize))
	if _, err := io.ReadFull(rand.Reader, ivRandom); err != nil {
		return nil, nil, err
	}
	hex.Encode(iv, ivRandom)

	// Initialize new crypter struct. Errors are ignored.
	crypter, err := utils.NewCrypter(key, iv)
	if err != nil {
		return nil, nil, err
	}

	// Lets encode plaintext using the same key and iv.
	// This will produce the very same result: "RanFyUZSP9u/HLZjyI5zXQ=="
	encoded, err := crypter.Encrypt(text)
	if err != nil {
		return nil, nil, err
	}

	return encoded, iv, nil
}
