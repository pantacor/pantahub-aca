// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package enroll

import (
	"errors"
	"log"
	"net/http"

	"github.com/emicklei/go-restful"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/pantacor/pantahub-aca/storage"
	"gitlab.com/pantacor/pantahub-aca/utils"
)

// GetEnroll return a certificate from a given ID
func GetEnroll(request *restful.Request, response *restful.Response) {
	defer func() {
		if r := recover(); r != nil {
			log.Println(r)
			utils.ErrorResponse(response, errors.New("Cert not found"), http.StatusNotFound, "")
		}
	}()

	id := request.PathParameter("enroll-id")
	st := storage.NewStorage()

	cert, err := st.FindCert(bson.ObjectIdHex(id))
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "")
		return
	}

	response.WriteEntity(&cert)
}
