// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package enroll

import (
	"crypto"
	"crypto/x509"
	"encoding/base64"
	"errors"
	"net/http"

	"github.com/emicklei/go-restful"
	"github.com/google/go-tpm/tpm2"
	"gitlab.com/pantacor/pantahub-aca/models"
	"gitlab.com/pantacor/pantahub-aca/storage"
	"gitlab.com/pantacor/pantahub-aca/utils"
)

type erDecoded struct {
	AkPub    tpm2.Public
	EkPub    tpm2.Public
	EkCert   *x509.Certificate
	EkPubBin []byte
	AkPubKey crypto.PublicKey
	AkName   string
}

// KeyEnroll function to EnrollKey key to ACA
func KeyEnroll(request *restful.Request, response *restful.Response) {
	var enrollRequest EnrollmentRequest

	err := request.ReadEntity(&enrollRequest)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "decode enroll request")
		return
	}

	err = validateRequest(&enrollRequest)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "validateRequest")
		return
	}

	enrollData, err := decodeEnrollRequest(&enrollRequest)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "decodeEnrollRequest")
		return
	}

	// Validates the AK public part of the Payload agains the Ak name in the payload
	err = validateAkPublic(enrollData.AkPub, enrollData.AkName)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "validateDecodePublic")
		return
	}

	// Validates that the EK Certificate has the same public key as the ek pub on the payload
	err = validateCertAndPub(enrollData.EkCert, enrollData.EkPub)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "validateCertAndPub EK")
		return
	}

	// Validate that the EK certificate has been signed by a know root of trust for TPM ek certificates
	err = verifyEk(enrollData.EkCert)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "verifyEk")
		return
	}

	// Create a credentials to be used has encryptation for the IAK certificate
	credential, secret, err := createCredentials(enrollData)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "createCredentials")
		return
	}

	st := storage.NewStorage()
	cert := st.CreateCert(&models.Cert{
		Type: models.AIKType,
	})

	akName, err := utils.Tpm2PublicName(enrollData.AkPub)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "Tpm2PublicName")
		return
	}

	// Needs to create certificate
	certificate, err := createCertificate(enrollData.AkPubKey, cert.ID.Hex(), akName)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "createCertificate")
		return
	}

	// Encrypt the IAK certificate using the secret created by the TPM create credential command
	encCertificate, iv, err := encrypt(certificate, secret)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "encrypt")
		return
	}

	cert.Cert = base64.StdEncoding.EncodeToString(certificate)
	err = st.InsertCert(cert)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "InsertCert")
		return
	}

	body := EnrollmentResponse{
		Credential:  base64.StdEncoding.EncodeToString(credential),
		Certificate: base64.StdEncoding.EncodeToString(encCertificate),
		Iv:          string(iv),
	}

	response.WriteEntity(&body)
}

func validateRequest(enrollRequest *EnrollmentRequest) error {
	if enrollRequest.AkPub == "" {
		return errors.New(RequestEnrollErrors.AkPubRequired)
	}
	if enrollRequest.AkName == "" {
		return errors.New(RequestEnrollErrors.AkNameRequired)
	}
	if enrollRequest.EkCert == "" {
		return errors.New(RequestEnrollErrors.EkCertRequired)
	}
	if enrollRequest.EkPub == "" {
		return errors.New(RequestEnrollErrors.EkPubRequired)
	}
	return nil
}
