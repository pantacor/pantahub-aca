// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package enroll

// EnrollmentRequest Enroll key request struct
type EnrollmentRequest struct {
	AkPub  string `json:"ak-pub" description:"Ak Public Key"`
	AkName string `json:"ak-name" description:"Ak name"`
	EkCert string `json:"ek-cert" description:"Ek Certificate"`
	EkPub  string `json:"ek-pub" description:"Ek Public Key"`
}

// EnrollmentResponse enroll process response
type EnrollmentResponse struct {
	Certificate string `json:"certificate" description:"Certificate"`
	Credential  string `json:"credential" description:"Secret certificate"`
	Iv          string `json:"iv" description:"iv"`
}
