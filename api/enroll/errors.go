// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package enroll

// RequestEnrollErr validation error for request enrollment
type RequestEnrollErr struct {
	AkPubRequired  string
	AkNameRequired string
	AkPrivRequired string
	EkPubRequired  string
	EkCertRequired string
	AKInvalid      string
}

// RequestEnrollErrors Posible validation error
var RequestEnrollErrors = &RequestEnrollErr{
	AkPubRequired:  "Ak public key is required",
	AkNameRequired: "Ak name is required",
	AkPrivRequired: "Ak private is required",
	EkPubRequired:  "Ek public is required",
	EkCertRequired: "Ek certificate is required",
	AKInvalid:      "Invalid AK public key",
}
