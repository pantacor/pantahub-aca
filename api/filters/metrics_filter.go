// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package filters

import (
	"strconv"
	"time"

	"github.com/emicklei/go-restful"
	"github.com/prometheus/client_golang/prometheus"
)

var responseTime *prometheus.HistogramVec = prometheus.NewHistogramVec(
	prometheus.HistogramOpts{
		Name: "api_response",
		Help: "Register api responses",
	},
	[]string{"endpoint", "method", "code"},
)

// MetricsFilter measure metrics
func MetricsFilter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	before := time.Now()
	chain.ProcessFilter(req, resp)
	after := time.Since(before)

	endpoint := req.SelectedRoutePath()
	method := req.Request.Method
	code := strconv.Itoa(resp.StatusCode())

	responseTime.WithLabelValues(endpoint, method, code).Observe(after.Seconds())
}

func init() {
	prometheus.MustRegister(responseTime)
}
