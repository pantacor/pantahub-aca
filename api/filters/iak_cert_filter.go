// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package filters

import (
	"net/http"

	"github.com/emicklei/go-restful"
	"gitlab.com/pantacor/pantahub-aca/utils"
)

// IAkCertFilterFilter process iak and set up in attibute
func IAkCertFilterFilter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	iakCert, err := utils.ParseHeaderPhClientCertificate(req.HeaderParameter("PhTpm2ClientIAKCert"))
	if err != nil {
		utils.ErrorResponse(resp, err, http.StatusInternalServerError, "IAK cert failed to parse")
		return
	}

	// Validate IAK cert
	err = utils.ValidateAcaSigned(iakCert)
	if err != nil {
		utils.ErrorResponse(resp, err, http.StatusInternalServerError, "IAK cert failed to verify")
		return
	}

	req.SetAttribute("PhIAKCert", iakCert)
	chain.ProcessFilter(req, resp)
}
