// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package filters

import (
	"crypto/x509"
	"errors"
	"net/http"
	"net/url"

	"github.com/emicklei/go-restful"
	"gitlab.com/pantacor/pantahub-aca/utils"
)

const (
	// HTTPHeaderPhClientCertificate pantahub client certificate
	HTTPHeaderPhClientCertificate = "Pantahub-TLS-Client-Cert"

	// HTTPHeaderPhProxyTLSToken pantahub proxy token
	HTTPHeaderPhProxyTLSToken = "Pantahub-TLS-Proxy-Token"
)

// TLSProxyCertFilter will ensure that calling clients have authenticated with a valid client
// IDevId certificate and will validate the extensions to it.
//
// If validation succeeds the key attributes will be put into the calling context to allow
// business logic to adjust behaviour based on what was found.
//
// This filter can operate in mode behind proxy or directly on TLS port. If we are opreating
// behind a proxy it is mandatory that the proxy authenticates itself to the backend in order
// to enable the code path that uses the "PhClientCertificate" Http header field to retrieve
// the client certificate used.
//
func TLSProxyCertFilter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	var cert *x509.Certificate
	var err error

	phProxyTLSUnlockAuth := req.HeaderParameter(HTTPHeaderPhProxyTLSToken)

	if phProxyTLSUnlockAuth != "" {
		if phProxyTLSUnlockAuth != utils.Env.ProxyTLSUnlockAuthToken {
			utils.ErrorResponse(resp, errors.New("No Valid "+utils.Env.ProxyTLSUnlockAuthToken+" provided"),
				http.StatusInternalServerError, "invalid proxy tlx token configuration")
			return
		}
		phCertificate := req.HeaderParameter(HTTPHeaderPhClientCertificate)
		if phCertificate != "" {
			// Nginx encode the client certificate using url escape instead of hex
			decodedValue, err := url.QueryUnescape(phCertificate)
			if err != nil {
				utils.ErrorResponse(resp, err, http.StatusInternalServerError, "parse client certificate error")
				return
			}

			cert, err = utils.ParsePEMCertString([]byte(decodedValue))
			if err != nil {
				utils.ErrorResponse(resp, err, http.StatusInternalServerError, "parse client certificate error")
				return
			}
		}
		req.SetAttribute("PhTlsCert", cert)
	} else if req.Request.TLS != nil {
		// if we are NOT behind proxy we extract directlty from TLS connection
		if req.Request.TLS != nil && len(req.Request.TLS.PeerCertificates) == 0 {
			err = errors.New("No TLS Certificate available through TLS session")
			utils.ErrorResponse(resp, err, http.StatusInternalServerError, "parse client certificate error")
			return
		}
		cert = req.Request.TLS.PeerCertificates[len(req.Request.TLS.PeerCertificates)-1]
		req.SetAttribute("PhTlsCert", cert)
	}
	chain.ProcessFilter(req, resp)
}
