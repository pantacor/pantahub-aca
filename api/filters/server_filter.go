// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package filters

import (
	"log"
	"time"

	"github.com/emicklei/go-restful"
)

// GlobalLogging Global Filter
func GlobalLogging(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	log.Printf("[%s] %s\n", req.Request.Method, req.Request.URL)
	chain.ProcessFilter(req, resp)
}

// MeasureTimeLog WebService (post-process) Filter (as a struct that defines a FilterFunction)
func MeasureTimeLog(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	now := time.Now()
	chain.ProcessFilter(req, resp)
	log.Printf("[response time] %v\n", time.Now().Sub(now))
}

// ResponseCodeLog WebService (post-process) Filter (as a struct that defines a FilterFunction)
func ResponseCodeLog(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	chain.ProcessFilter(req, resp)
	log.Printf("[response code] %v\n", resp.StatusCode())
	log.Printf("[response length] %v\n", resp.ContentLength())
}
