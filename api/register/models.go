// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package register

// LDevIDRegReq for the registration payload structure
type LDevIDRegReq struct {
	LDevID       []byte `json:"csr"`
	LDevIDPub    []byte `json:"ldev-pub"`
	PcrSelection string `json:"pcr-selection"`
}

// LDevIReqRes for the registration payload response
type LDevIReqRes struct {
	Certificate string `json:"crt"`
}

// IDevIDRegReq for the registration payload structure
type IDevIDRegReq struct {
	Certificate string `json:"csr"`
}

// IDevIReqRes for the registration payload response
type IDevIReqRes struct {
	Certificate string `json:"crt"`
}
