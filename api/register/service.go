// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package register

import (
	"net/http"

	restful "github.com/emicklei/go-restful"
	restfulspec "github.com/emicklei/go-restful-openapi"
	"gitlab.com/pantacor/pantahub-aca/api/filters"
)

const (
	// ServiceURL base endpoint URL
	ServiceURL = "/register"

	// LDevIDRegister register ldevid URL
	LDevIDRegister = "/{service}"

	// IDevIDRegister register ldevid URL
	IDevIDRegister = "/"
)

// NewService create new register service
func NewService() *restful.WebService {
	tags := []string{"register"}
	ws := new(restful.WebService)

	ws.Filter(filters.TLSProxyCertFilter).
		Filter(filters.IAkCertFilterFilter)

	ws.Path(ServiceURL).
		Consumes(restful.MIME_JSON).
		Produces(restful.MIME_JSON)

	ws.Route(ws.POST(LDevIDRegister).
		To(LDevIDWithService).
		Doc("Register a LDevId for a Service").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Operation("LDevIDWithService").
		Writes(map[string]interface{}{}).
		Returns(http.StatusOK, "OK", LDevIReqRes{}))

	ws.Route(ws.POST(IDevIDRegister).
		To(IDevIDReg).
		Doc("Register a IDevId").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Operation("IDevIDReg").
		Writes(map[string]interface{}{}).
		Returns(http.StatusOK, "OK", IDevIReqRes{}))

	return ws
}
