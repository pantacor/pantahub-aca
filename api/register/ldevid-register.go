// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package register

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"net/http"
	"net/url"

	restful "github.com/emicklei/go-restful"
	"github.com/google/go-tpm/tpm2"
	"gitlab.com/pantacor/pantahub-aca/api/certs"
	"gitlab.com/pantacor/pantahub-aca/models"
	"gitlab.com/pantacor/pantahub-aca/storage"
	"gitlab.com/pantacor/pantahub-aca/utils"
)

// LDevIDWithService register a LDevID for a service
func LDevIDWithService(request *restful.Request, response *restful.Response) {
	var requestPayload LDevIDRegReq

	r := request.Attribute("PhTlsCert")
	if r == nil {
		utils.ErrorResponse(response, errors.New("No PhTlsCert found in request; check your client and filters"),
			http.StatusInternalServerError, "no ph tls cert found")
	}

	iDevCert := r.(*x509.Certificate)

	if iDevCert == nil {
		utils.ErrorResponse(response, errors.New("No PhTlsCert found in request; check your client and filters"),
			http.StatusInternalServerError, "no ph tls cert found")
		return
	}

	err := utils.ValidateCASigned(iDevCert)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "invalid IDevID")
		return
	}

	err = request.ReadEntity(&requestPayload)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "ReadEntity")
		return
	}

	if requestPayload.LDevID == nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "no cert found")
		return
	}

	if requestPayload.LDevIDPub == nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "no public key found")
		return
	}

	if requestPayload.PcrSelection == "" {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "no pcr selection found")
		return
	}

	lDevIDCert, err := x509.ParseCertificate(requestPayload.LDevID)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "ParsePEMHexEncodeCert")
		return
	}

	lDevIDPub, err := tpm2.DecodePublic(requestPayload.LDevIDPub)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "DecodePublic Pub")
		return
	}

	iakCert := request.Attribute("PhIAKCert").(*x509.Certificate)

	exts, err := utils.ProcessPHExtentions(lDevIDCert)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "Extensions invalid")
		return
	}

	// check that csr is valid csr (self signed)
	// TPM self-signed certs, so we seed the cert itself in pool ...
	err = utils.ValidateCertificateSelfSigned(lDevIDCert)
	if err != nil {
		response.WriteErrorString(http.StatusBadRequest, utils.ErrorMessageJSON(err, "cert failed to verify"))
		return
	}

	// the IDevId cert was using (see attributes by tls_idev_id_filter)

	// validate that csr really belongs to that LDEVPub
	err = utils.ValidateTPMPublicKey(lDevIDCert.PublicKey.(*rsa.PublicKey), lDevIDPub)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "Invalidad public key")
		return
	}

	// check that csr includes tpm2_certify info that attests the ldev key
	// check that that attestation is happening with same AKCert as the
	err = utils.ValidateAttestationSignature(
		exts.Raw.CertifyAttest,
		exts.CertifySignature.RSA.Signature,
		iakCert.PublicKey.(*rsa.PublicKey))
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "Attestation Signature invalid")
		return
	}

	// check LDevID quote and validate
	err = utils.ValidateAttestationSignature(
		exts.Raw.QuoteAttest,
		exts.QuoteSignature.RSA.Signature,
		iakCert.PublicKey.(*rsa.PublicKey))
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "Quote invalid")
		return
	}

	st := storage.NewStorage()
	cert := st.CreateCert(&models.Cert{
		Type: models.LDevIDType,
	})

	certificate, err := signCertificate(lDevIDCert, iakCert.PublicKey.(*rsa.PublicKey), cert.ID.Hex())
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "sign certificate fail")
		return
	}

	cert.Cert = base64.StdEncoding.EncodeToString(certificate)
	err = st.InsertCert(cert)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "InsertCert")
		return
	}

	body := &LDevIReqRes{
		Certificate: cert.Cert,
	}

	response.WriteEntity(body)
}

func signCertificate(cert *x509.Certificate, publicKey *rsa.PublicKey, certID string) ([]byte, error) {
	acaCertificate, err := base64.StdEncoding.DecodeString(utils.Env.AcaCertificate)
	if err != nil {
		return nil, err
	}

	acaPrivate, err := base64.StdEncoding.DecodeString(utils.Env.AcaPrivateKey)
	if err != nil {
		return nil, err
	}

	acaCert, err := x509.ParseCertificate(acaCertificate)
	if err != nil {
		return nil, err
	}

	pemPrivateBlock, _ := pem.Decode(acaPrivate)
	if pemPrivateBlock == nil {
		return nil, errors.New("Aca private key missing")
	}

	privateKey, err := utils.ParsePrivateKey(pemPrivateBlock.Bytes)
	if err != nil {
		return nil, err
	}

	certURL, err := certs.BuildCertURL(certID)
	if err != nil {
		return nil, err
	}

	cert.ExtraExtensions = cert.Extensions
	cert.URIs = []*url.URL{certURL}

	return x509.CreateCertificate(
		rand.Reader,
		cert,
		acaCert,
		publicKey,
		privateKey)
}
