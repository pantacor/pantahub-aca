// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package register

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"net/http"

	restful "github.com/emicklei/go-restful"
	"github.com/google/go-tpm/tpm2"
	"gitlab.com/pantacor/pantahub-aca/services/pantahub"
	"gitlab.com/pantacor/pantahub-aca/utils"
)

// IDevIDReg create a new device using the IDevID certificate
func IDevIDReg(request *restful.Request, response *restful.Response) {
	var requestPayload IDevIDRegReq

	err := request.ReadEntity(&requestPayload)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "ReadEntity")
		return
	}

	if requestPayload.Certificate == "" {
		utils.ErrorResponse(response, errors.New("IDevID is required certificate"), http.StatusBadRequest, "no idevid")
		return
	}

	certReqPem, err := base64.StdEncoding.DecodeString(requestPayload.Certificate)
	if err != nil {
		utils.ErrorResponse(response, errors.New("IDevID is required certificate"), http.StatusBadRequest, "no idevid")
		return
	}

	certBlock, _ := pem.Decode(certReqPem)
	iDevCert, err := x509.ParseCertificateRequest(certBlock.Bytes)
	if err != nil {
		utils.ErrorResponse(response, errors.New("IDevID is required as TLS certificate"), http.StatusBadRequest, "no idevid")
		return
	}

	err = iDevCert.CheckSignature()
	if err != nil {
		utils.ErrorResponse(response, errors.New("IDevID signature invalid"), http.StatusBadRequest, "no idevid")
		return
	}

	// now we go for the TPM specific validations, that are:
	// * validate that IAK cert (from PhTpm2ClientIAKCert header)
	//   is issued by trusted authority (aka aca.pantahub.com)
	// * validate attest signature in the IDevId with IAK pub key (extracted from the IAK Cert)
	// * validate NAME (extracted through DecodeAttestationData of the attest data in the IDevId Certificate extension)
	//   matches the name IDevId TPMT_PUBLIC part (from PhTpm2ClientIDevPub header)
	// * validate that public key of IDevId used as TLS client matches the public key
	//   in the IDevId public part

	// for that we expect following out of band verification assets in headers:
	//   PhTpm2ClientIAKCert: uri encoded pem
	//   PhTpm2ClientIDevPub: the TPMT_PUBLIC part of the IDevId
	iDevPubString, err := hex.DecodeString(request.HeaderParameter("PhTpm2ClientIDevPub"))
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "error decoding hex from PhTpm2ClientIDevPub")
		return
	}

	iDevPub, err := tpm2.DecodePublic(iDevPubString)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "IDev TPMT_PUBLIC invalid")
		return
	}

	// Validate IDevID certificate public key
	err = utils.ValidateTPMPublicKey(iDevCert.PublicKey.(*rsa.PublicKey), iDevPub)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "IDevID cert doesn't match")
		return
	}

	extensions, err := utils.ProcessPHExtensionsCsr(iDevCert)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "Invalid extensions on certificate")
		return
	}

	iakCert := request.Attribute("PhIAKCert").(*x509.Certificate)

	// Validate CertifyInfo Attestation Signature
	err = utils.ValidateAttestationSignature(
		extensions.Raw.CertifyAttest,
		extensions.CertifySignature.RSA.Signature,
		iakCert.PublicKey.(*rsa.PublicKey))
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "Attestation Signature invalid")
		return
	}

	// extract certifiedName and QName of the iDevPub
	_, _, err = utils.GetCertifiedName(extensions, iDevPub)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "Attestation Name invalid")
		return
	}

	// Validate OwnerShip
	iDevIDName, err := pantahub.ValidateOwnership(extensions, iDevPub)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusInternalServerError, "Invalid ownership")
		return
	}

	payload, err := pantahub.RegisterIDevID(iDevCert, iDevIDName, iakCert.Subject.SerialNumber)
	if err != nil {
		utils.ErrorResponse(response, err, http.StatusBadRequest, "Can't create the device")
		return
	}

	response.WriteAsJson(payload)
}
