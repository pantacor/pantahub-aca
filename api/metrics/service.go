// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package metrics

import (
	"net/http"

	"github.com/emicklei/go-restful"
	restfulspec "github.com/emicklei/go-restful-openapi"
)

const (
	// GetURL endpoint URL
	GetURL = "/"

	// ServiceURL base endpoint URL
	ServiceURL = "/metrics"
)

// NewService create metrics service
func NewService() *restful.WebService {
	tags := []string{"metrics"}
	ws := new(restful.WebService)
	ws.Path(ServiceURL).
		Produces("text/plain")

	ws.Route(ws.GET(GetURL).To(GetMetrics).
		Doc("List metrics").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Operation("GetMetrics").
		Returns(http.StatusOK, "OK", ""))

	return ws
}
