// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package health

import (
	"net/http"

	"github.com/emicklei/go-restful"
	restfulspec "github.com/emicklei/go-restful-openapi"
	"gitlab.com/pantacor/pantahub-aca/models"
)

const (
	// GetURL endpoint URL
	GetURL = "/"

	// ServiceURL base endpoint URL
	ServiceURL = "/health"
)

// NewService create new enrollment service
func NewService() *restful.WebService {
	tags := []string{"health"}
	ws := new(restful.WebService)

	ws.Path(ServiceURL).
		Consumes(restful.MIME_JSON).
		Produces(restful.MIME_JSON)

	ws.Route(ws.GET(GetURL).To(GetHealth).
		Doc("API Health endpoint").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Operation("GetHealth").
		Writes(models.Cert{}).
		Returns(http.StatusOK, "OK", models.Cert{}))

	return ws
}
