default: test clean

test:
	docker-compose build --force-rm --pull
	docker-compose run --rm -T api go test ./...

clean:
	docker-compose down -v --remove-orphans

ci:
	echo "TODO"
