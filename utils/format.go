// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package utils

import (
	"encoding/base64"
	"encoding/pem"
	"fmt"

	"github.com/google/go-tpm/tpm2"
)

const (
	pemFormatExtension = "crt"
)

// FromBase64DerToExtension get a certificate with base64 encode and formated
func FromBase64DerToExtension(base64Cert, format string) ([]byte, error) {
	certDer, err := base64.StdEncoding.DecodeString(base64Cert)
	if err != nil {
		return nil, err
	}

	var cert []byte

	switch format {
	case pemFormatExtension:
		cert = pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: certDer})
	default:
		cert = certDer
	}

	return cert, nil
}

// Tpm2PublicName get tpm name as string
func Tpm2PublicName(pub tpm2.Public) (string, error) {
	name, err := pub.Name()
	if err != nil {
		return "", nil
	}
	return Tpm2NameToString(name), nil
}

// Tpm2NameToString convert tpm2.Name to string
func Tpm2NameToString(name tpm2.Name) string {
	return fmt.Sprintf("%04x%x", name.Digest.Alg, name.Digest.Value)
}
