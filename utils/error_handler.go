// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package utils

import (
	"encoding/json"
	"log"

	"github.com/emicklei/go-restful"
	"gitlab.com/pantacor/pantahub-aca/models"
)

// ErrorResponse get the server response and generate a proper json response
func ErrorResponse(resp *restful.Response, err error, status int, code string) {
	message := ErrorMessageJSON(err, code)
	resp.WriteErrorString(status, message)
}

// ErrorMessageJSON return a rest error with the message
func ErrorMessageJSON(err error, code string) string {
	log.Printf("[error: %s] %s\n", code, err.Error())

	jsonError := &models.RestError{
		Message: err.Error(),
		Code:    code,
	}

	message, err := json.MarshalIndent(jsonError, "", "	")
	if err != nil {
		return err.Error()
	}

	return string(message)
}
