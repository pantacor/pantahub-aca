// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package utils

import (
	"bytes"
	"crypto"
	"crypto/aes"
	"crypto/cipher"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/asn1"
	"encoding/base64"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"fmt"
	"strconv"

	"github.com/google/go-tpm/tpm2"
	"gitlab.com/pantacor/pantahub-aca/models"
)

// Crypter create Crypter
type Crypter struct {
	key   []byte
	iv    []byte
	block cipher.Block
}

type rawExtensions struct {
	CertifyAttest    []byte
	CertifySignature []byte
	QuoteAttest      []byte
	QuoteSignature   []byte
}

// PHExtensions pantacor certificate extensions
type PHExtensions struct {
	CertifyAttest    *tpm2.AttestationData
	CertifySignature *tpm2.Signature
	QuoteAttest      *tpm2.AttestationData
	QuoteSignature   *tpm2.Signature
	QuotePcrList     []byte
	OwnerTokenID     []byte
	Owner            string
	NameSigByOwner   []byte
	Raw              *rawExtensions
}

// PhCertExtensionIDs all the indentifiers for pantahub extensions on a certificate values
var PhCertExtensionIDs = &models.PhCertExtensions{
	AIKName:       asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 54621, 100, 0},
	OwnerPrnOID:   asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 54621, 100, 1},
	OwnernameSig:  asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 54621, 100, 2},
	TokenID:       asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 54621, 100, 3},
	CertifyAttest: asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 54621, 100, 4},
	CertifySig:    asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 54621, 100, 5},
	QuoteAttest:   asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 54621, 100, 6},
	QuoteSig:      asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 54621, 100, 7},
	QuotePcrList:  asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 54621, 100, 8},
	DevicePRN:     asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 54621, 100, 9},
}

// ParsePEMCertString parse a pem certificate
func ParsePEMCertString(pemCert []byte) (*x509.Certificate, error) {
	block, _ := pem.Decode(pemCert)
	if block == nil {
		return nil, errors.New("no valid pem")
	}

	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, err
	}

	return cert, nil
}

// ParsePEMHexEncodeCert parse a hex encoded certificate
func ParsePEMHexEncodeCert(encodeCert string) (*x509.Certificate, error) {
	pemCert, err := hex.DecodeString(encodeCert)
	if err != nil {
		return nil, err
	}

	return ParsePEMCertString(pemCert)
}

// ParsePEMBase64EncodeCert parse a hex encoded certificate
func ParsePEMBase64EncodeCert(encodeCert string) (*x509.Certificate, error) {
	pemCert, err := base64.StdEncoding.DecodeString(encodeCert)
	if err != nil {
		return nil, err
	}

	return ParsePEMCertString(pemCert)
}

// ParseHeaderPhClientCertificate get a URL encode pem certificate
func ParseHeaderPhClientCertificate(header string) (*x509.Certificate, error) {
	if header == "" {
		return nil, nil
	}

	return ParsePEMHexEncodeCert(header)
}

// GetTrustedRoots return and pool of trusted root credentials for TPM
func GetTrustedRoots() (*x509.CertPool, error) {
	roots := x509.NewCertPool()

	for _, root := range Env.TrustedRoots {
		certDer, err := base64.StdEncoding.DecodeString(root)
		if err != nil {
			return nil, err
		}

		cert, err := x509.ParseCertificate(certDer)
		if err != nil {
			return nil, err
		}

		cert.UnhandledCriticalExtensions = []asn1.ObjectIdentifier{}

		roots.AddCert(cert)
	}

	return roots, nil
}

// GetTrustedRootsFromBundle return and pool of trusted root credentials for TPM
func GetTrustedRootsFromBundle() (*x509.CertPool, error) {
	roots := x509.NewCertPool()

	bundle, err := base64.StdEncoding.DecodeString(Env.TrustedRootsBundle)
	if err != nil {
		return nil, err
	}

	ok := roots.AppendCertsFromPEM(bundle)
	if !ok {
		return nil, errors.New("Certs from bundle can't be loaded")
	}

	return roots, nil
}

// NewCrypter define new crypter
func NewCrypter(key []byte, iv []byte) (*Crypter, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	blockSize := block.BlockSize()
	if len(iv) != blockSize {
		return nil, errors.New("IV length has to be equal to: " + strconv.Itoa(blockSize))
	}

	return &Crypter{key, iv, block}, nil
}

// Encrypt Encrypt
func (c *Crypter) Encrypt(plainText []byte) ([]byte, error) {
	origData := pkcs5Padding(plainText, c.block.BlockSize())
	blockMode := cipher.NewCBCEncrypter(c.block, c.iv)
	cryted := make([]byte, len(origData))
	blockMode.CryptBlocks(cryted, origData)
	return cryted, nil
}

// Decrypt decrypt
func (c *Crypter) Decrypt(cipherText []byte) ([]byte, error) {
	blockMode := cipher.NewCBCDecrypter(c.block, c.iv)
	origData := make([]byte, len(cipherText))
	blockMode.CryptBlocks(origData, cipherText)
	origData = pkcs5UnPadding(origData)
	return origData, nil
}

// ParsePrivateKey parse private key from DER content
func ParsePrivateKey(der []byte) (crypto.PrivateKey, error) {
	if key, err := x509.ParsePKCS1PrivateKey(der); err == nil {
		return key, nil
	}

	if key, err := x509.ParsePKCS8PrivateKey(der); err == nil {
		switch key := key.(type) {
		case *rsa.PrivateKey, *ecdsa.PrivateKey:
			return key, nil
		default:
			return nil, fmt.Errorf("Found unknown private key type in PKCS#8 wrapping")
		}
	}

	if key, err := x509.ParseECPrivateKey(der); err == nil {
		return key, nil
	}

	return nil, fmt.Errorf("Failed to parse private key")
}

// ParsePublicKey parse public key from DER content
func ParsePublicKey(der []byte) (crypto.PublicKey, error) {
	if key, err := x509.ParsePKCS1PublicKey(der); err == nil {
		return key, nil
	}
	if key, err := x509.ParsePKIXPublicKey(der); err == nil {
		switch key := key.(type) {
		case *rsa.PublicKey, *ecdsa.PublicKey:
			return key, nil
		default:
			return nil, fmt.Errorf("Found unknown public key type in PKCS#8 wrapping")
		}
	}

	return nil, fmt.Errorf("Failed to parse public key")
}

// ValidateCertificateSelfSigned validate that a certificate is self signed
func ValidateCertificateSelfSigned(cert *x509.Certificate) error {
	rootPool := x509.NewCertPool()
	rootPool.AddCert(cert)

	_, err := cert.Verify(x509.VerifyOptions{
		Roots: rootPool,
	})

	return err
}

// ValidateAcaSigned validate a certificate that has been signed by pantahub ACA
func ValidateAcaSigned(cert *x509.Certificate) error {
	acaCertificate, err := base64.StdEncoding.DecodeString(Env.AcaCertificate)
	if err != nil {
		return err
	}

	acaCert, err := x509.ParseCertificate(acaCertificate)
	if err != nil {
		return err
	}

	rootPool := x509.NewCertPool()
	rootPool.AddCert(acaCert)

	_, err = cert.Verify(x509.VerifyOptions{
		Roots: rootPool,
	})

	return err
}

// ValidateCASigned validate a certificate that has been signed by pantahub CA
func ValidateCASigned(cert *x509.Certificate) error {
	acaCertificate, err := base64.StdEncoding.DecodeString(Env.CaCert)
	if err != nil {
		return err
	}

	acaCert, err := x509.ParseCertificate(acaCertificate)
	if err != nil {
		return err
	}

	rootPool := x509.NewCertPool()
	rootPool.AddCert(acaCert)

	_, err = cert.Verify(x509.VerifyOptions{
		Roots: rootPool,
	})

	return err
}

// GetCertifiedName get and validate certificate name agains TPMB_PUBLIC part
func GetCertifiedName(exts *PHExtensions, public tpm2.Public) (name *tpm2.Name, qname *tpm2.Name, err error) {
	var certMatchesPub bool
	certifiedName := exts.CertifyAttest.AttestedCertifyInfo.Name

	// names should be equal
	certMatchesPub, err = certifiedName.MatchesPublic(public)
	if err != nil {
		return nil, nil, err
	}

	if certMatchesPub {
		name = &certifiedName
		qname = &exts.CertifyAttest.AttestedCertifyInfo.QualifiedName
	} else {
		err = errors.New("certifiedname does not match public")
		return nil, nil, err
	}

	return name, qname, nil
}

// ValidateTPMPublicKey validate a tpm public key
func ValidateTPMPublicKey(pubKey *rsa.PublicKey, pubPart tpm2.Public) error {
	tpmKey, err := pubPart.Key()
	if err != nil {
		return err
	}

	key1, err := x509.MarshalPKIXPublicKey(tpmKey.(*rsa.PublicKey))
	if err != nil {
		return err
	}

	key2, err := x509.MarshalPKIXPublicKey(pubKey)
	if err != nil {
		return err
	}

	if !bytes.Equal(key1, key2) {
		return errors.New("Public keys doesn't match")
	}

	return nil
}

// ValidateAttestationSignature validate certification signature
func ValidateAttestationSignature(attestation, signature []byte, akPub *rsa.PublicKey) error {
	hashed := sha256.Sum256(attestation)
	return rsa.VerifyPKCS1v15(akPub, crypto.SHA256, hashed[:], signature)
}

// ProcessPHExtensionsCsr process all pantacor extensions if they exists
func ProcessPHExtensionsCsr(cert *x509.CertificateRequest) (*PHExtensions, error) {
	extensions := &PHExtensions{
		Raw: &rawExtensions{},
	}

	for _, ext := range cert.Extensions {
		switch id := ext.Id.String(); id {
		case PhCertExtensionIDs.OwnernameSig.String():
			extensions.NameSigByOwner = ext.Value

		case PhCertExtensionIDs.OwnerPrnOID.String():
			extensions.Owner = string(ext.Value)

		case PhCertExtensionIDs.CertifyAttest.String():
			extensions.Raw.CertifyAttest = ext.Value

		case PhCertExtensionIDs.CertifySig.String():
			extensions.Raw.CertifySignature = ext.Value

		case PhCertExtensionIDs.TokenID.String():
			extensions.OwnerTokenID = ext.Value

		case PhCertExtensionIDs.QuoteAttest.String():
			extensions.Raw.QuoteAttest = ext.Value

		case PhCertExtensionIDs.QuoteSig.String():
			extensions.Raw.QuoteSignature = ext.Value

		case PhCertExtensionIDs.QuotePcrList.String():
			extensions.QuotePcrList = ext.Value
		default:
		}
	}

	if len(extensions.Raw.CertifyAttest) > 0 {
		certifyAttest, err := tpm2.DecodeAttestationData(extensions.Raw.CertifyAttest)
		if err != nil {
			return nil, err
		}
		extensions.CertifyAttest = certifyAttest
	}

	if len(extensions.Raw.CertifySignature) > 0 {
		certifySignature, err := tpm2.DecodeSignature(bytes.NewBuffer(extensions.Raw.CertifySignature))
		if err != nil {
			return nil, err
		}
		extensions.CertifySignature = certifySignature
	}

	if len(extensions.Raw.QuoteAttest) > 0 {
		quoteAttest, err := tpm2.DecodeAttestationData(extensions.Raw.QuoteAttest)
		if err != nil {
			return nil, err
		}
		extensions.QuoteAttest = quoteAttest
	}

	if len(extensions.Raw.QuoteSignature) > 0 {
		quoteSignature, err := tpm2.DecodeSignature(bytes.NewBuffer(extensions.Raw.QuoteSignature))
		if err != nil {
			return nil, err
		}
		extensions.QuoteSignature = quoteSignature
	}

	return extensions, nil
}

// ProcessPHExtentions process all pantacor extensions if they exists
func ProcessPHExtentions(cert *x509.Certificate) (*PHExtensions, error) {
	extensions := &PHExtensions{
		Raw: &rawExtensions{},
	}

	for _, ext := range cert.Extensions {
		switch id := ext.Id.String(); id {
		case PhCertExtensionIDs.OwnernameSig.String():
			extensions.NameSigByOwner = ext.Value

		case PhCertExtensionIDs.OwnerPrnOID.String():
			extensions.Owner = string(ext.Value)

		case PhCertExtensionIDs.CertifyAttest.String():
			extensions.Raw.CertifyAttest = ext.Value

		case PhCertExtensionIDs.CertifySig.String():
			extensions.Raw.CertifySignature = ext.Value

		case PhCertExtensionIDs.TokenID.String():
			extensions.OwnerTokenID = ext.Value

		case PhCertExtensionIDs.QuoteAttest.String():
			extensions.Raw.QuoteAttest = ext.Value

		case PhCertExtensionIDs.QuoteSig.String():
			extensions.Raw.QuoteSignature = ext.Value

		case PhCertExtensionIDs.QuotePcrList.String():
			extensions.QuotePcrList = ext.Value
		default:
		}
	}

	if len(extensions.Raw.CertifyAttest) > 0 {
		certifyAttest, err := tpm2.DecodeAttestationData(extensions.Raw.CertifyAttest)
		if err != nil {
			return nil, err
		}
		extensions.CertifyAttest = certifyAttest
	}

	if len(extensions.Raw.CertifySignature) > 0 {
		certifySignature, err := tpm2.DecodeSignature(bytes.NewBuffer(extensions.Raw.CertifySignature))
		if err != nil {
			return nil, err
		}
		extensions.CertifySignature = certifySignature
	}

	if len(extensions.Raw.QuoteAttest) > 0 {
		quoteAttest, err := tpm2.DecodeAttestationData(extensions.Raw.QuoteAttest)
		if err != nil {
			return nil, err
		}
		extensions.QuoteAttest = quoteAttest
	}

	if len(extensions.Raw.QuoteSignature) > 0 {
		quoteSignature, err := tpm2.DecodeSignature(bytes.NewBuffer(extensions.Raw.QuoteSignature))
		if err != nil {
			return nil, err
		}
		extensions.QuoteSignature = quoteSignature
	}

	return extensions, nil
}

func pkcs5Padding(src []byte, blockSize int) []byte {
	padding := blockSize - len(src)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(src, padtext...)
}

func pkcs5UnPadding(src []byte) []byte {
	length := len(src)
	unpadding := int(src[length-1])
	return src[:(length - unpadding)]
}
